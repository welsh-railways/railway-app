#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import json



#engine = create_engine('postgresql://gis:gis@localhost/gis', echo=True)
#engine = create_engine('postgresql+psycopg2://mash:mash@127.0.0.1:5433/wr', echo=True)

SRID = 4326

NS = "{http://www.opengis.net/kml/2.2}"
pth = "/home/railways/welsh-railways.gitlab.io/docs/anglesey-central"

fn = pth + "/test.kml"

print(fn)
import xml.etree.ElementTree as et

doc = et.parse(fn)

nmsp = '{http://www.opengis.net/kml/2.2}'

print("------------------------------")
for pm in doc.iterfind('.//{0}Placemark'.format(nmsp)):
    print(">>" , pm.find('{0}name'.format(nmsp)).text)

    # #.iterfind('.//{0}Placemark'.format(NS)):
    # lineStrings = doc.findall('.//{0}LineString'.format(NS))# './/{http://earth.google.com/kml/2.1}LineString')
    # for attributes in lineStrings:
    #     for subAttribute in attributes:
    #         if subAttribute.tag == '{http://earth.google.com/kml/2.1}coordinates':
    #             print(subAttribute.tag, subAttribute.text)

    lines = pm.findall('{0}LineString/{0}coordinates'.format(nmsp))
    #for ls in pm.iterfind('{0}MultiGeometry/{0}LineString/{0}coordinates'.format(nmsp)):
    #print(lines)
    for ls in lines:
        points = ls.text.strip().replace('\n','').split(" ")
        for p in points:
            print("   p=", p.split(","))





sys.exit(0)

# from osgeo import gdal, ogr
#
# srcDS = gdal.OpenEx(fn)
#
json_file = 'anglesey-central.geojson'
# ds = gdal.VectorTranslate(json_file, srcDS, format='GeoJSON')
# #
# # print(ds)
# # print(ds.GetRasterCount())
# #
# #
# # for d in ds:
# #     print(d)


with open(json_file, "r", buffering=10000000) as f:
    data = json.load(f)
    #con = f.read()
    #print("YESSS", con)

print("ROOT=", data.keys())
#data = json.loads(r'%s' % con)

for feat in data['features']:
    #print(feat['geometry']['type'], feat.keys())
    goem = feat['geometry']
    typ = goem['type']
    if typ == "Point":
        latlng = goem['coordinates']
        dic = dict(name = feat['properties']['name'],
                   type = "station",
                   lat = latlng[0], lng = latlng[1])

        print(dic )

    else:
        print(feat['geometry']['type'], feat.keys())

if False:


    from pykml import parser



    with open(fn, "r") as f:

        doc = parser.parse(f)

    print(doc)

    if False:
        for el in doc.iter("*"):
            print(el.tag)
            if el.tag == NS + "Placemark":
                print("====", el.name, el.Point)

    #for e in doc.Document.Folder.Placemark:
    #  coor = e.Point.coordinates.text.split(',')
    for pm in doc.iterfind('.//{0}Placemark'.format(NS)):
        name = pm.find('{0}name'.format(NS))
        print("name=", name)
        #print(pm, type(pm))
        #point = pm.findall('.//{0}Point'.format(NS))[0]
        #print(point)
        #print(pm.find('{0}Point'.format(NS)).text)




    #foo = doc.findall(NS + "Placemark")
    #for e in doc.Document.Folder.iter('Placemark'):
    #     print(e.tag)
    #
    # for el in foo:
    #     for el in doc.iter("*"):
    #         print(el.tag)
    #     print(el)