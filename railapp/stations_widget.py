# -*- coding: utf-8 -*-
# @author: Peter Morgan <pete@daffodil.uk.com>

import os

from Qt import QtGui, QtCore, Qt, QtWidgets, pyqtSignal
import frontmatter
import requests
import bs4

import xwidgets
import xtree
from img import Ico
import ut

import railib
from railapp import PROJECT_PATH



class C:
    node = 0

class StationsWidget( QtWidgets.QWidget ):

    sigFolder = pyqtSignal(object)

    def __init__( self, parent=None):
        super().__init__()


        self.mainLayout = xwidgets.vlayout()
        self.setLayout( self.mainLayout )


        self.toolbar = xwidgets.XToolBar()
        self.mainLayout.addWidget(self.toolbar)

        self.actUpdateIndex = self.toolbar.addAction( "Stations.md", self.write_stations_index)
        #self.actUpdateIndex = self.toolbar.addAction("Up Disused", self.up_disused)
        self.actUpdateIndex = self.toolbar.addAction("Site.idx", self.on_write_site_kml)
        #self.actUpdateIndex = self.toolbar.addAction("Site.idx", self.wr)

        #self.actUpdateIndex = self.toolbar.addAction("Check Disused", self.check_disused)

        #self.actWiki = self.toolbar.addAction("Wiki", self.wiki_search)


        self.tree = QtWidgets.QTreeWidget()
        self.mainLayout.addWidget(self.tree)


        hi = self.tree.headerItem()
        hi.setText(0, "Stations")

        self.tree.itemClicked.connect(self.on_item_clicked)
        #self.tree.itemDoubleClicked.connect(self.on_item_double)


        #self.scan_dirs()
        self.statusBar = xwidgets.StatusBar(self, refresh=self.scan_dirs)
        self.mainLayout.addWidget(self.statusBar)

        self.scan_dirs()


    def on_item_clicked(self, item, col):
        ss = item.text(C.node)
        self.sigFolder.emit(ss)



    def scan_dirs(self):
        print("scan_dirs")
        self.tree.clear()

        for sdir in sorted(os.listdir(railib.STATIONS_PATH)):
            pth = os.path.join(railib.STATIONS_PATH, sdir)
            #print(r, PROJECT_PATH + r)

            if True: #os.path.isdir(pth):
                #kml = os.path.join(pth, "%s.kml" % pth)

                ico =  Ico.folder
                item = xtree.XTreeWidgetItem()
                item.setText(C.node, sdir)
                item.set_ico(C.node, ico)
                self.tree.addTopLevelItem(item)

        self.tree.sortByColumn(C.node, Qt.AscendingOrder)

    def write_stations_index(self):


        railib.write_stations_index()
        return

        toc = []
        idxx = {}

        for sdir in sorted(os.listdir(STATIONS_PATH)):
            pth = os.path.join(STATIONS_PATH, sdir, "index.md")
            print(pth)
            if not os.path.exists(pth):
                continue
            with open(pth) as f:
                data = frontmatter.load(f)
                #print("data=", data['meta'])


                n = data['meta']['name']

                aa = n[0]
                if not aa in idxx:
                    idxx[aa] = []

                #lst.append(dict(path=sdir, name=n))
                uu = "- [%s](/station/%s/)" %(n, sdir)
                toc.append(uu)
                idxx[aa].append(uu)

        s = "Stations Index\n================\n\n"

        #s += "\n".join(toc)
        for aa, items in idxx.items():
            s += "## %s\n\n" % aa.upper()

            s += "\n".join(items)
            s += "\n\n"

        s += "\n\n"

        stations_md = os.path.join(PROJECT_PATH, "stations.md")
        ut.write_file(stations_md, s)





    def up_disused(self):


       railib.fetch_disused()


    def on_tree_selection(self, item, col):

        ss = item.text(C.node)
        self.wiki_search(ss)





    def on_item_double(self, item, col):

        self.sigFolder.emit(item.text(C.node))

    def on_write_site_kml(self):

        railib.gen_site_not_human()
