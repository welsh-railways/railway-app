
# -*- coding: utf-8 -*-
# @author: Peter Morgan <pete@daffodil.uk.com>

import os

from Qt import QtGui, QtCore, Qt, QtWidgets, pyqtSignal

import frontmatter

import xwidgets
import xtree
from img import Ico

import railib

class C:
    key = 0
    val = 1

class StationsDBWidget( QtWidgets.QWidget ):

    sigFolder = pyqtSignal(object)

    def __init__( self, parent=None):
        super().__init__()


        self.mainLayout = xwidgets.vlayout()
        self.setLayout( self.mainLayout )


        self.toolbar = xwidgets.XToolBar()
        self.mainLayout.addWidget(self.toolbar)

        #self.actUpdateIndex = self.toolbar.addAction( "Stations.md", self.write_stations_index)
        #self.actUpdateIndex = self.toolbar.addAction("Up Disused", self.up_disused)
        #self.actUpdateIndex = self.toolbar.addAction("Site.idx", self.on_write_site_kml)
        #self.actUpdateIndex = self.toolbar.addAction("Site.idx", self.wr)

        #self.actUpdateIndex = self.toolbar.addAction("Check Disused", self.check_disused)

        #self.actWiki = self.toolbar.addAction("Wiki", self.wiki_search)


        self.tree = QtWidgets.QTreeWidget()
        self.mainLayout.addWidget(self.tree)
