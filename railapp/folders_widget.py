# -*- coding: utf-8 -*-
# @author: Peter Morgan <pete@daffodil.uk.com>

import os

from Qt import QtGui, QtCore, Qt, QtWidgets, pyqtSignal

import xwidgets
import xtree
from img import Ico

from railapp import PROJECT_PATH

class C:
    node = 0

class FoldersWidget( QtWidgets.QWidget ):

    sigFolder = pyqtSignal(object)

    def __init__( self, parent=None):
        super().__init__()


        self.mainLayout = xwidgets.vlayout()
        self.setLayout( self.mainLayout )


        self.tree = QtWidgets.QTreeWidget()
        self.mainLayout.addWidget(self.tree)

        hi = self.tree.headerItem()
        hi.setText(0, "Folder")

        self.tree.itemClicked.connect(self.on_item_clicked)

        #self.scan_dirs()
        self.statusBar = xwidgets.StatusBar(self, refresh=self.scan_dirs)
        self.mainLayout.addWidget(self.statusBar)


    def on_item_clicked(self, item, col):
        self.sigFolder.emit(item.text(C.node))


    def scan_dirs(self):

        self.tree.clear()

        for sdir in os.listdir(PROJECT_PATH):
            pth = os.path.join(PROJECT_PATH, sdir)
            #print(r, PROJECT_PATH + r)

            if os.path.isdir(pth):
                kml = os.path.join(pth, "%s.kml" % sdir)

                ico = Ico.kmlf if os.path.exists(kml) else Ico.folder
                item = xtree.XTreeWidgetItem()
                item.setText(C.node, sdir)
                item.set_ico(C.node, ico)
                self.tree.addTopLevelItem(item)