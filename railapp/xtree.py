# -*- coding: utf-8 -*-


from Qt import Qt, QtGui, QtCore, QtWidgets
import json

from img import Ico

JSON_ROLE = Qt.UserRole + 13

class XTreeWidgetItem( QtWidgets.QTreeWidgetItem ):
    """An extended QTreeWidgetItem with added functionality for sorting and formatting"""
    def __init__( self, parent=None ):
        super().__init__( parent )


    def DEAD__lt__( self, otherItem ):
        """Reimplemented function for less than

        The L{TS_ROLE} contains dats if date is a set_date"""
        column = self.treeWidget().sortColumn()

        data = self.data(column, SORT_ROLE)
        if data:
            """
            if data.toString().startsWith( "i=" ):
                orig_i = self.data( column, SORT_ROLE ).toInt() 
                other_i = otherItem.data( column, SORT_ROLE ).toInt()
                return orig_i < other_i
            """
            if data.startswith( "s=" ):

                orig_str = str( self.data( column, SORT_ROLE ).toString() ).split( "=" )[1]
                other_string = str( otherItem.data( column, SORT_ROLE ).toString() ).split( "=" )[1]
                return ut._human_key( orig_str ) < ut._human_key( other_string )

        #data = self.data( column, SORT_ROLE )
        #if data:
            if data.startswith( "ts=" ):
                orig_str = str( self.data( column, SORT_ROLE ) ).split( "=" )[1]
                osstr = str( otherItem.data( column, SORT_ROLE ) ).split( "=" )
                #print orig_str, osstr
                if len(osstr) == 0 or len(osstr) == 1:
                    return False
                #print osstr
                other_str = osstr[1]
                #print "orig/other", orig_str, other_str
                #print "ints", float(orig_str) , float(other_str), float(orig_str) < float(other_str)
                return orig_str < other_str

        orig = self.text( column ).lower()
        other = otherItem.text( column ).lower()

        return ut.human_key( orig ) < ut.human_key( other )

    ##======================================================================
    ## Date Related
    def set_date( self, col, date_str, text=None, fuzzy=True, no_text=False, short=True, fg=None, bg=None ):
        """Sets the date of a col.
          - This also sets the timestamp for sorting.
          - A normal date is displayed, unless the text is set

        @todo: implement fuzzy

        @param col: column index
        @param date_str: Date string or object
        @param text: Optional custom text that overides formatting
        """

        if not date_str:
            self.setText( col, "-" )
        if not no_text:
            if text == None:
                self.setText( col, xdate.dmy( date_str ) )
            else:
                self.setText( col, text )
        else:
            self.setText( col, "-" )
        #self.set_ts( col, date_str )
        self.setData( col, SORT_ROLE, "ts=%s" % xdate.to_ts( date_str ) )
        self.setData( col, DATE_ROLE, xdate.to_mysql_date( date_str ) )
        if fg:
            self.setForeground(col, QtGui.QColor(fg))
        if bg:
            self.setBackground(col, QtGui.QColor(bg))

    def set_date_time( self, col, date_str, text=None, fuzzy=True, no_text=False ):
        """Sets the date of a col.
          - This also sets the timestamp for sorting.
          - A normal date is displayed, unless the text is set

        @todo: implement fuzzy

        @param col: column index
        @param date_str: Date string or object
        @param text: Optional custom text that overides formatting
        """
        if not date_str:
            self.setText(col, "-")
            # self.set_ts( col, date_str )
            self.setData(col, SORT_ROLE, "")
            self.setData(col, DATE_ROLE, "")
            return

        if no_text == False:
            if text == None:
                if fuzzy:
                    txt = xdate.fuzzy(date_str, with_time=True )
                #else:
                txt = xdate.dmyhm( date_str )
                self.setText( col, txt )

            else:
                self.setText( col, text )
        else:
            self.setText( col, "-" )
        #self.set_ts( col, date_str )
        self.setData( col, SORT_ROLE, "ts=%s" % xdate.to_ts( date_str ) )
        self.setData( col, DATE_ROLE, xdate.to_mysql_date( date_str ) )

    def pydate( self, col ):
        return xdate.to_py_date( str( self.data( col, DATE_ROLE ).toString() ) )

    def date( self, col ):
        return xdate.to_mysql_date( str( self.data( col, DATE_ROLE ).toString() ) )

    def date_range( self ):
        lst = []
        for col in range( 1, 8 ):
            lst.append( self.date( col ) )
        slst = sorted( lst )
        return slst

    def set_ico(self, col, ico):
        self.setIcon(col, Ico.icon(ico))

    ##======================================================================
    ## Formatting
    def set_num( self, col, no_str, places=0, currency='', zero="-", data=None ):
        if isinstance(no_str, str) or isinstance(no_str, unicode):
            xx = G.ut.to_no(no_str)
        else:
            xx = no_str
        self.setText( col, G.format.num( xx, places=places, currency=currency, zero="-" ) )
        self.align_right( col )
        if data:
            self.setData(col, QtCore.Qt.UserRole, data)

    def get_data(self, col):
        vari = self.data(col, QtCore.Qt.UserRole)
        if vari == None:
            return None
        return str( vari.toString() )

    def set_json(self, col, data):
        self.setData(col, JSON_ROLE, json.dumps(data))

    def json(self, col):
        s = self.data(col, JSON_ROLE)
        if s:
            s = str(s)
        return json.loads(s)

    def set_text( self, col, strr, sort=None ):
        if isinstance( strr, int ):
            s = "%s" % strr
        else:
            s = strr.strip()
        self.setText( col, s )

        if sort:
            self.setData( col, SORT_ROLE, "s=%s" % sort )


    def set( self, col, strr, bold=False, align=None, fontSize=None, fg=None, bg=None, ico=None, icon=None ):

        s = ""
        if strr != None:
            s = str(strr).strip()

        self.setText( col, s.replace("\n", " ") )

        if ico:
            self.set_ico(col, ico)
        if icon:
            self.setIcon(col, icon)

        if bold:
            self.set_bold(col, bold)
        if align:
            self.setTextAlignment(col, align)

        if fg:
            self.setForeground(col, QtGui.QColor(fg))
        if bg:
            self.setBackground(col, QtGui.QColor(bg))

        f = self.font(col)
        if fontSize:
            f.setPointSize(fontSize)

        self.setFont(col, f)


    def s(self, col):
        return str(self.text(col))

    def i(self, col):
        return ut.to_int(self.text(col))

    def b(self, col):
        x = self.i(col)
        return x == 1

    def set_int( self, col, i, text=None, sort=False, zero=""):

        if text:
            self.setText(col, text)
        else:
            if i == None:
                self.setText( col, zero )
            else:
                self.setText( col, str(i) )
        if sort:
            if i < 0:
                ii = (i * -1) + 1000
            else:
                ii = i + 2000
            s = "s=%04d" % ii
            self.setData( col, SORT_ROLE, s )
            #self.setText(col,  s)

    def set_checked(self, col, state, color=None):
        #print "set_item__checked", col, state, color, QtCore.Qt.Checked if state else QtCore.Qt.Unchecked
        self.setCheckState(col, QtCore.Qt.Checked if state else QtCore.Qt.Unchecked)
        if color:
            if state:
                self.set_cell_bg(col, "#009900")
            else:
                self.set_cell_bg(col, "white")

    def get_checked(self, col):
        return self.checkState(col) == QtCore.Qt.Checked

    def get_int(self, col):
        x = self.text(col)
        if x == "" or x == "-":
            return None
        return int(x)

    def set_id( self, s_id ):
        self.setData( 0, ID_ROLE, s_id )

    def id( self ):
        v, ok = self.data( 0, ID_ROLE ).toInt()
        return v

    def set_row_strike(self, state):
        for c in  range(0, self.columnCount() ):
            f = self.font(c)
            f.setStrikeOut(state)
            self.setFont(c, f)

    ##==============================================================
    def set_row_bg( self, color ):
        for col_idx in range( 0, self.columnCount() ):
            self.setBackground( col_idx, QtGui.QColor( color ) )

    def set_row_fg( self, color ):
        for col_idx in range( 0, self.columnCount() ):
            self.setForeground( col_idx, QtGui.QColor( color ) )


    def set_cell_bg( self, col_idx, color ):
        self.setBackground( col_idx, QtGui.QColor( color ) )

    def set_cell_fg( self, col_idx, color ):
        self.setForeground( col_idx, QtGui.QColor( color ) )

    def set_font_family( self, col, fam):
        font = self.font( col )
        font.setFamily( fam )
        self.setFont( col, font )

    def set_font_size(self, col, size):
        font = self.font( col )
        font.setPointSize(size)
        self.setFont( col, font )

    def set_bold( self, col_lst, bold=True ):

        if isinstance(col_lst, list):
            for c in col_lst:
                self._set_bold(c, bold)
        else:
            self._set_bold(col_lst, bold)

    def set_italic( self, col_lst, italic=True ):

        if isinstance(col_lst, list):
            for c in col_lst:
                self._set_italic(c, italic)
        else:
            self._set_italic(col_lst, italic)

    def _set_bold( self, col, bold):
        font = self.font( col )
        font.setBold( bold )
        self.setFont( col, font )

    def _set_italic( self, col, italic):
        font = self.font( col )
        font.setItalic( italic )
        self.setFont( col, font )

    def set_row_bold( self, bold=True ):
        for col_idx in range( 0, self.columnCount() ):
            self.set_bold(col_idx, bold)

    def lbl_checked(self, col, val, bgcolor=None ):
        self.setTextAlignment(col, QtCore.Qt.AlignCenter)
        if bgcolor == None:
            bgcolor = "#FFECAA"
        if bool(val):
            self.setText(col, "Yes")
            self.set_cell_bg(col, bgcolor)
        else:
            self.setText(col, "-")
            self.set_cell_bg(col, "#efefef")


    ##==============================================================
    def align_right( self, col ):
        self.setTextAlignment( col, QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter )

    def align_center( self, col ):
        self.setTextAlignment( col, QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter )


    def pathToRoot(self, item, col):
        ret = []
        while item:
            #nPath = NPath()
            #nPath.text = str(item.text(col)),
            #nPath.icon = item.icon(col)
            ret.append(
                    {'text': str(item.text(col)),
                    "icon": QtWidgets.QIcon(item.icon(col)) }
            )
            item = item.parent()
        ret.reverse()
        return ret



    def DEADset_drop_enabled(self):
        flags = self.flags()
        flags = flags|QtCore.Qt.ItemIsDropEnabled



class XTreeView(QtWidgets.QTreeView):

    def __init__( self, parent=None, sorting=True, stretch_last=True):
        super().__init__(parent)


        self.setRootIsDecorated( False )
        self.setUniformRowHeights ( True )
        self.setAlternatingRowColors( True )
        self.setExpandsOnDoubleClick( False )
        self.header().setStretchLastSection(stretch_last)

        self.setSortingEnabled( sorting )

        self.setSelectionBehavior(QtWidgets.QAbstractItemView.SelectRows)
        self.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)

        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)

    def set_settings(self, ki):

        self._settings_ki = ki

        G.settings.restore_tree(self)

        self.header().sectionResized.connect(self.on_header_resized)
        #self.connect(self.header(), QtCore.SIGNAL("sectionResized(int,int,int)"), self.on_header_resized)

    def on_header_resized(self, idx, old, new):
        G.settings.ssave_tree(self)


    def setColumnsHidden(self, cols, hidden=True):
        for c in cols:
            self.setColumnHidden(c, hidden)

    def resizeColumnsToContents(self, cols):
        for c in cols:
            self.resizeColumnToContents(c)

    def update_widths(self):

        model = self.model().sourceModel()
        for idx, col in enumerate(model.col_defs):
            #print(idx, col, col.resize)
            if col.resize:
                self.resizeColumnToContents(idx)

    def select(self, midx):
        ism = QtCore.QItemSelectionModel
        self.selectionModel().select(midx, ism.SelectCurrent|ism.Rows)
        self.scrollTo(midx, QtWidgets.QAbstractItemView.EnsureVisible )


class XTreeWidget(QtWidgets.QTreeWidget):

    def __init__( self, parent=None):
        QtWidgets.QTreeWidget.__init__( self, parent)

        #self.header().setMovable(False)
        self._settings_ki = None


    def set_settings(self, ki):

        self._settings_ki = ki

        G.settings.restore_tree(self)
        self.tree.header().sectionResized.connect(self.on_header_resized)

    def on_header_resized(self, idx, old, new):
        G.settings.save_tree(self)

    def setColumnsHidden(self, lst, state=True):
        for c in lst:
            self.setColumnHidden(c, state)
