# -*- coding: utf-8 -*-
# @author: Peter Morgan <pete@daffodil.uk.com>

import os

from Qt import QtGui, QtCore, Qt, QtWidgets, pyqtSignal

import frontmatter

import xwidgets
import xtree
from img import Ico

import railib

class C:
    key = 0
    val = 1

class StationWidget( QtWidgets.QWidget ):

    #sigFolder = pyqtSignal(object)

    def __init__( self, parent=None, folder=None):
        super().__init__()

        self.folder = None
        self.meta = {}

        self.mainLayout = xwidgets.vlayout()
        self.setLayout( self.mainLayout )

        self.toolbar = xwidgets.XToolBar()
        self.mainLayout.addWidget(self.toolbar)

        #self.actUpdateIndex = self.toolbar.addAction("Write Index", self.write_index)
        # self.actUpdateIndex = self.toolbar.addAction("Get Disused", self.get_disused)

        #self.actUpdateIndex = self.toolbar.addAction("Check Disused", self.check_disused)

        self.actWiki = self.toolbar.addAction("Wiki", self.on_wiki_search)
        self.actDisused = self.toolbar.addAction("Disused", self.on_disused)
        self.actDisused.setDisabled(True)

        self.tree = QtWidgets.QTreeWidget()
        self.mainLayout.addWidget(self.tree)

        hi = self.tree.headerItem()
        hi.setText(C.key, "Key")
        hi.setText(C.val, "Val")

        self.tree.itemClicked.connect(self.on_item_clicked)

        #self.scan_dirs()
        self.statusBar = xwidgets.StatusBar(self, refresh=self.load_station)
        self.mainLayout.addWidget(self.statusBar)




    def on_item_clicked(self, item, col):
        self.sigFolder.emit(item.text(C.node))


    def load_station(self, folder):

        self.folder = folder
        self.tree.clear()

        print("load_station-=", folder)
        self.meta, err = railib.read_frontmatter(railib.STATIONS_PATH, folder)
        #print(data.keys())
        # print("data=", data['meta'])
        #self.meta = data['meta']
        if False:
            if not "position" in self.meta:
                self.meta['position'] = None
            else:
                if not "elevation" in self.meta['position']:
                    self.meta['position']['elevation'] = None

            if not "status" in self.meta:
                self.meta['status'] = None
            if not "disused" in self.meta:
                self.meta['disused'] = None
        #print("meta===", self.meta)
        self.add_node(self.tree.invisibleRootItem(), self.meta)
        #self.actDisused.setEnabled(True if railib.get_disused(self.folder) else False)
        self.tree.resizeColumnToContents(C.key)

    def add_node(self, pitem, data):

        #data = meta['data']

        for key, val in data.items():
            #val = data[key]
            #print("===", type(val), key, val)
            if isinstance(val, dict):
                xItem = xtree.XTreeWidgetItem(pitem)
                xItem.setText(C.key, key)
                self.add_node(xItem, val)
                xItem.setExpanded(True)


            elif isinstance(val, list):
                xItem = xtree.XTreeWidgetItem(pitem)
                xItem.setText(C.key, key)
                #self.add_node(xItem, val)
                xItem.setExpanded(True)
                for dic in val:
                    xxitem = xtree.XTreeWidgetItem(xItem)
                    print("===", dic)
                    self.add_node(xxitem, dic)
                    xxitem.setExpanded(True)

            else:
                item = xtree.XTreeWidgetItem(pitem)
                item.setText(C.key, key)
                if val == None:
                    item.setText(C.val, "")
                    item.set_cell_bg(C.val, "pink")
                else:
                    item.setText(C.val, str(val))
            #self.tree.addTopLevelItem((item))



    def on_wiki_search(self):
        print("------------")
        res, err = railib.wiki_search(self.folder)
        print(res)

        name, titles, summary, links = res
        print("=====", name, titles, summary, links)
        for idx, tit in enumerate(titles):
            print(">>", tit, links[idx])

        #for r in res:
        #    print("=====", r)

    def on_disused(self):
        pass