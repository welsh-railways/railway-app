
import re
import json
import yaml
from Qt import QtCore, QtWidgets

def to_json(data):
    return json.dumps(data, indent=4)

def read_file(file_path):
    with open(file_path, "r") as xfile:
        return xfile.read()
    return None

def write_file(file_path, content):
    with open(file_path, "w") as xfile:
        xfile.write(content)
        xfile.close()
    return None

def read_json_file(file_path):
    with open(file_path, "r") as xfile:
        try:
            return json.load(xfile), None
        except Exception as e:
            return None, str(e)
    return None, "ERROR om read_json"

def write_json_file(file_path, data):
     s = json.dumps( data, indent=4, sort_keys=True)
     write_file(file_path, s)


def write_yaml_file(file_path, data):
    with open(file_path, "w") as f:
        s = yaml.dump(data, f)


def to_int(v, default=None):
    try:
        return int(v)
    except Exception as e:
        pass

    return default



def validate_email( email ):
    if email == None:
        return False

    email = str(email).strip()
    if email == "":
        return False

    if len(email) < 7:
        return False
    #em = parseaddr(email)
    #print em
    if email.find(" ") != -1:
        return False

    if email.find("@") == -1:
        return False

    pattern = r'[\w\.-]+@[\w\.-]+(\.[\w]+)+'
    if re.match(pattern, email):
        return True
    #print "no match"
    return False

def validate_mobile(s):
    return True


def xsplit(s, splitter=" ", lower=False):

    s = s.strip()
    if lower:
        s = s.lower()

    p = []
    for snip in s.split(splitter):
        ss = snip.strip()
        if ss != "":
            p.append(ss)
    return p

def title(s):
    return s.replace("_", " ").title()

def autosize_dialog( window, width_margin=100, height_margin=150,
                     max_width=None, max_height=None):
    """Auto size a window to width/height margin

    @param window: Window Object to resize
    @type window: QWidget
    @param width_margin: Requested width margin of window
    @param height_margin: Requested height margin or window
    """
    #print("----------------------")
    ##desktop = QtWidgets.QApplication.desktop()
    desktop = QtWidgets.QApplication.desktop()
    screen_no = desktop.screenNumber(window)
    if G.settings.is_dev():
        screen_no = 1

    #print("screen=", screen_no)
    rect = desktop.screenGeometry(screen_no)

    wid = rect.width() - width_margin
    #wid = wid - width_margin

    #if max_width and wid > max_width:
    #    # print "set max"
    #    wid = max_width
    # print rect.width(), width_margin, wid - width_margin, max_width, wid
    # print rect, screen_no

    hei = rect.height() - height_margin
    #print(rect)
    #print(wid, hei)
    #hei = hei - height_margin
    #if max_height and hei > max_height:
    #    hei = max_height
    # print height_margin, rect.height(), max_height, hei
    window.resize(wid, hei)


def desktop_open_file(pth):
    url = QtCore.QUrl(pth)
    url.setScheme("file")
    QtGui.QDesktopServices.openUrl(url)

##================================================================
FILESIZE_SUFFIXES = {
    1000: ['KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
    1024: ['Kb', 'Mb', 'Gb', 'Tb', 'Pb', 'Eb', 'Zb', 'Yb']
    #1024: ['k', 'm', 'g', 't', 'p', 'Eb', 'Zb', 'Yb']
}

def format_bytes( size, a_kilobyte_is_1024_bytes=False ):
    """Convert a file size to human-readable form.
    @param size: file size in bytes
    @type size: long
    @param a_kilobyte_is_1024_bytes: If True (default), use multiples of 1024
                                if False, use multiples of 1000
    @return: Formatted string
    @rtype: str
    """
    if size < 0:
        return ''

    multiple = 1024 if a_kilobyte_is_1024_bytes else 1000
    for suffix in FILESIZE_SUFFIXES[multiple]:
        size /= multiple
        if size < multiple:
            #return '{0:.1f} {1}'.format( size, suffix )
            return '{0:.0f} {1}'.format( size, suffix )

    return '---'

def a2z():
    return [chr(chNum) for chNum in list(range(ord('a'),ord('z')+1))]

def slug(foo, whitespace="_"):
    return foo.strip().lower().replace(" ", whitespace)
