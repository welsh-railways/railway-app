

import os
from Qt import  QtWidgets, QtGui, QtCore


# see https://github.com/spyder-ide/qtawesome

import qtawesome as qta
#from pyqtx.xico import Ico as XIco

ICON_DIR = os.path.abspath(os.path.join("static", 'icons'))


class Ico():

    favicon = "favicon.svg"
    # db = "fa5s.database"
    # db_table = "fa5s.table"
    #
    # caret_down = "fa5s.angle-down"
    # arrow_left = "fa5s.arrow-left"
    # arrow_right = "fa5s.arrow-right"

    #
    #
    #
    add = "fa5s.plus"
    edit = "fa5.edit"
    delete = "fa5.trash-alt"

    clear = "fa5s.caret-right"  # ??
    close = "ei.remove"
    cancel = "ei.remove"
    save = "ei.ok-circle"

    refresh = "ei.refresh"

    filter_on = "bullet_green.png"
    filter_off = "bullet_black.png"

    filter_active = "ui-check-box.png"
    filter_all = "ui-check-box-mix.png"
    filter_inactive = "ui-check-box-uncheck.png"

    arrow_prev = "fa5s.arrow-left"
    arrow_next = "fa5s.arrow-right"
    arrow_select = "fa5s.angle-down"
    arrow_up = "ei.arrow-up"
    arrow_down = "ei.arrow-down"

    details = "fa5s.file"

    black = 'bullet_black.png'
    blue = 'bullet_blue.png'
    green = 'bullet_green.png'
    red = 'bullet_red.png'
    purple = 'bullet_purple.png'

    login = "ei.lock"
    logout = "ei.lock"  # ??

    help = "fa5s.caret-square-down"
    help_page = "fa5s.caret-square-down"

    settings = "ei.lock"  # ??
    upgrade = "fa5s.download"
    permissions = "fa5s.download"

    map = "map.png"
    #-------------------

    catagories = "document-hf.png"
    category = "document-hf.png"



    wizard = "fa5s.hat-wizard"

    calendar = "fa5s.calendar-alt"


    time_sheet = "clock-select.png"


    flag = ["fa5s.exclamation-triangle", "red"]


    file_sys_browser = ["fa5.folder-open", "#333333"]
    file_copy = 'page_white_copy.png'
    file_delete = 'page_white_delete.png'
    file_open = 'page_white_go.png'
    file_rename = 'page_white_edit.png'
    files_select = "folder_page.png"
    file_send = "email_attach.png"

    folders = 'folders-stack.png'
    folder = ['ei.folder', "#999999"]
    folder_add = 'folder_add.png'
    folder_add2 = "folder--plus.png"
    folder_copy = 'folder_page.png'
    folder_delete = 'folder_delete.png'
    folder_open = 'folder_go.png'
    folder_rename = 'folder_edit.png'


    upload = 'drive-upload.png'

    kmlf = ["ei.folder", "red"]

    ext_excel = 'page_white_excel.png'
    ext_excel_olde = 'page_excel.png'

    ext_pdf = 'page_white_acrobat.png'
    ext_power_point = 'page_white_powerpoint.png'
    ext_text = 'page_white_text.png'
    ext_word = 'page_white_word.png'
    ext_zip = 'page_white_compressed.png'
    ext_unknown = 'blue-document-hf.png'

    @staticmethod
    def i(obj, color=None):
        return Ico.icon(obj, color=color)

    @staticmethod
    def icon(obj, color=None):
        xcolor = color

        if isinstance(obj, list):
            xname = obj[0]
            xcolor = obj[1]
        else:
            xname = obj

        if xname.endswith(".png") or xname.endswith(".svg"):
            pth = os.path.join(ICON_DIR, xname)
            return QtGui.QIcon(pth)
        #print("obj=", obj, xcolor)
        return qta.icon(xname, color=xcolor)


    @staticmethod
    def logo():
        pth = os.path.join(ICON_DIR, "favicon.svg")
        return QtGui.QIcon(pth)

    @staticmethod
    def pixmap(name, size=16):
        qicon = Ico.icon(name)
        return qicon.pixmap(QtCore.QSize(size, size))



    @staticmethod
    def from_contact_rec( dic):
        """Return icon associated with a contact
        @param dic: Contact record
        @type dic: dict
        @param return_string: Return icon as filename string instead of a qIcon
        @type return_string: bool
        @param is_staff: Return diffeerent Icon for staff interface
        @type is_staff: bool
        @return: Icon for batch
        @rtype: QIcon or string
        """
        active = bool( dic['con_active'] )
        is_mb = bool(dic['is_mailbox'])
        #if is_staff:
        if active == False:

            return Ico.icon( Ico.mailbox_inactive if is_mb else Ico.contact_inactive )

        if is_mb:
            return  Ico.icon( Ico.mailbox )

        if dic['root']:
            return Ico.icon(Ico.contact_staff)
            #if dic['pub']:
            #	return Ico.icon( Ico.ContactOnline )
        return Ico.icon( Ico.contact_active )

    @staticmethod
    def from_account_rec(rec):
        if rec.get('root'):
            return Ico.i(Ico.favicon)
        if rec["sole_trader_id"]:
            return Ico.i(Ico.contact)
        return Ico.i(Ico.account if rec['acc_active'] else Ico.black)

    @staticmethod
    def from_address_rec(rec):
        #if rec.get('root'):
        #    return Ico.i(Ico.favicon)
        if rec["is_branch"]:
            return Ico.i(Ico.address)
        return Ico.i(Ico.map)

    @staticmethod
    def file_type( ext ):
        """Return icon for given file type
        @params ext: file_extention
        @return: Icon for filetype
        @rtype: QIcon
        """

        ext = str(ext).lower()

        ico = EXT_MAP.get(ext)
        if not ico:
            ico = Ico.ext_unknown
        return Ico.icon( ico )



EXT_MAP = {



        'doc': Ico.ext_word,

        'ppt': Ico.ext_power_point,
        'pdf': Ico.ext_pdf,
        "txt": Ico.ext_text,

        'xls': Ico.ext_excel_olde,
        'xlsx': Ico.ext_excel,

        "zip":  Ico.ext_zip


}
