
ORG_NAME = "Welsh Railways"

DOMAIN = "welsh-railways.gitlab.io"


import os
import sys
import json
## Define paths

ROOT_PATH = os.path.abspath( os.path.join(os.path.dirname( __file__ ), ".."))

HERE_PATH = os.path.abspath( os.path.dirname( __file__ ))
if sys.path.count(HERE_PATH) == 0:
    sys.path.insert(0, HERE_PATH)


PROJECT_ROOT = "/home/railways/welsh-railways.gitlab.io"
PROJECT_PATH = "/home/railways/welsh-railways.gitlab.io/docs"

TEMP_PATH = os.path.join(ROOT_PATH, "__temp__")

if not os.path.exists(TEMP_PATH):
    os.makedirs(TEMP_PATH)


