# -*- coding: utf-8 -*-


from Qt import QtCore, QtGui, QtWidgets, Qt, pyqtSignal
from img import Ico


DEFAULT_SPACING = 0
DEFAULT_MARGIN = 0
DEFAULT_BUTTON_WIDTH = 80

DEFAULT_LINE_SIZE = 10

PROP_KEY = "key"


def get_object_name(obj):
    """Returns and objects name from objectName or class"""
    n = obj.objectName()
    if n:
        return n
    n = obj.__class__.__name__
    if n:
        return n
    assert None == None

#=====================================================
# Layouts

def hlayout(spacing=DEFAULT_SPACING, margin=DEFAULT_MARGIN):
    """Convenience function to create a QHBoxLayout"""
    lay = QtWidgets.QHBoxLayout()
    if isinstance(margin, bool):
        margin = DEFAULT_SPACING
    if isinstance(spacing, bool):
        spacing = DEFAULT_SPACING
    lay.setContentsMargins(margin, margin, margin, margin)
    lay.setSpacing(spacing)
    return lay

def vlayout(spacing=DEFAULT_SPACING, margin=DEFAULT_MARGIN):
    """Convenience function to create a QVBoxLayout"""
    lay = QtWidgets.QVBoxLayout()
    if isinstance(margin, bool):
        margin = DEFAULT_SPACING
    if isinstance(spacing, bool):
        spacing = DEFAULT_SPACING
    lay.setContentsMargins(margin, margin, margin, margin)
    lay.setSpacing(spacing)
    return lay



class XButtonGroup(QtWidgets.QButtonGroup):
    def __init__( self, parent=None, clicked=None, exclusive=True):
        super().__init__(parent)

        self.setExclusive(exclusive)
        if clicked:
            self.buttonClicked.connect(clicked)


class XToolButton( QtWidgets.QToolButton ):

    sigDoubleClicked = pyqtSignal()

    def __init__( self, parent=None, colors=None, style=None,
                    autoRaise=True, menu=None,
                    text="", tooltip=None, ico=None, iconTop=False, iconSize=16,
                    bold=False, disabled=False,
                    width=None, popup=None, split=None,
                    clicked=None, key=None,
                    both=True, checkable=False
                    ):

        super().__init__(parent)

        self.colors = colors


        if key:
            self.setProperty(PROP_KEY, key)

        if width:
            self.setFixedWidth(width)

        if style:
            self.setStyleSheet(style)

        if disabled:
            self.setDisabled(True)


        if both:
            if iconTop:
                self.setToolButtonStyle( Qt.ToolButtonTextUnderIcon)
            else:
                self.setToolButtonStyle( Qt.ToolButtonTextBesideIcon)
        else:
            self.setToolButtonStyle( Qt.ToolButtonIconOnly)

        if checkable:
            self.setCheckable(True)

        if clicked:
            self.clicked.connect(clicked)

        if tooltip:
            self.setToolTip(tooltip)

        if text:
            self.setText(text)

        #== Font
        # Try and avoid styleSheet maybe
        if bold:
            self.setBold(bold)
        # < font

        if ico:
            self.setIco(ico)
            if iconSize:
                if isinstance(iconSize, list):
                    self.setIconSize( QtCore.QSize(iconSize[0],iconSize[0]))
                else:
                    self.setIconSize( QtCore.QSize(iconSize, iconSize))

        self.setAutoRaise(autoRaise)
        #assert popup != None and split != None

        if popup is not None:
            self.setPopupMode(QtWidgets.QToolButton.InstantPopup)
            menu=True
        if split is not None:
            self.setPopupMode(QtWidgets.QToolButton.MenuButtonPopup)
            menu = True

        if menu:
            self.setMenu(QtWidgets.QMenu())


    def key(self):
        return self.property(PROP_KEY)

    def setIco(self, ico, iconSize=16):
        self.setIcon(Ico.icon(ico))
        self.setIconSize( QtCore.QSize(iconSize, iconSize))

    def setEnabled(self, state):
        if self.colors:
            color = self.colors[1] if state else self.colors[0]
            self.setStyleSheet("background-color: %s" % color)
        QtWidgets.QToolButton.setEnabled(self, state)

    def setBold(self, state):
        f = self.font()
        f.setBold(state)
        self.setFont(f)

    def mouseDoubleClickEvent(self, ev):
        self.sigDoubleClicked.emit()

    def setChecked(self, state, block=None):
        if block:
            super().blockSignals(True)
        super().setChecked(state)
        if block:
            super().blockSignals(False)

class Spacer( QtWidgets.QWidget ):

    def __init__( self, parent=None, width=None ):
        super().__init__()

        if width:
            self.setFixedWidth(width)
        else:
            sp = QtWidgets.QSizePolicy()
            sp.setHorizontalPolicy( QtWidgets.QSizePolicy.Expanding )
            self.setSizePolicy( sp )



class CancelButton( QtWidgets.QPushButton ):
    def __init__( self, parent, text="Cancel", clicked=None ):
        super().__init__()

        self.setText( text )
        self.setIcon( Ico.icon( Ico.cancel ) )

        if clicked is not None:
            if clicked == True:
                self.clicked.connect(parent.on_cancel)
                return
            self.clicked.connect(clicked)



class SaveButton( QtWidgets.QPushButton ):
    def __init__( self, parent, text="Save", ico=None, clicked=None, width=None):
        super().__init__()

        self.dirty = False
        if width:
            self.setMinimumWidth( width )
        self.setText( text )
        self.setIcon( Ico.i( ico if ico else Ico.save ) )

        if clicked:
            self.clicked.connect(clicked)
        else:
            self.clicked.connect(parent.on_save)

    def set_dirty(self):
        self.dirty = True
        self.up_style()

    def clear_dirty(self):
        self.dirty = False
        self.up_style()

    def up_style(self):
        c = "pink" if self.dirty else "white"
        self.setStyleSheet("background-color: %s;" % c)

class DeleteButton( QtWidgets.QPushButton ):
    def __init__( self, parent, text="Delete", clicked=None ):
        super().__init__()

        self.setText( text )
        self.setIcon( Ico.icon( Ico.delete ) )
        if clicked:
            self.clicked.connect(clicked)
            return
        self.clicked.connect(parent.on_delete)

class XPushButton( QtWidgets.QPushButton ):
    def __init__( self, parent=None, text=None, clicked=None, ico=None ):
        super().__init__()

        self.setText( text )
        if ico:
            self.setIcon( Ico.icon( ico ) )
        if clicked:
            self.clicked.connect(clicked)

class HelpButton(XToolButton):

    def __init__(self, parent=None):
        super().__init__(parent, ico=Ico.help)



class FormActionBar(QtWidgets.QWidget):

    def __init__(self, parent, text="Save", clicked=None,
                 delete=False, cancel=True, refresh=True, clear=None,
                 pk=None, uid=None):
        super().__init__()#

        self.pk = pk
        self.uid = uid

        self.outerLayout = vlayout(margin=5, spacing=5)
        self.setLayout(self.outerLayout)

        self.outerLayout.addSpacing(10)

        lbl = XLabel(style="background-color: #cccccc;")
        lbl.setFixedHeight(1)
        self.outerLayout.addWidget(lbl)

        vlay = vlayout()
        self.outerLayout.addLayout(vlay)

        self.mainLayout = hlayout(margin=5, spacing=5)
        vlay.addLayout(self.mainLayout)


        if delete:
            self.buttDelete = DeleteButton(parent)
            self.mainLayout.addWidget(self.buttDelete)


        self.statusBar = StatusBar(self, refresh=refresh, mode=False)
        self.mainLayout.addWidget(self.statusBar)

        #self.mainLayout.addStretch(1)
        if cancel is not None:
            if cancel == False:
                pass
            elif cancel == True:
                self.buttCancel = CancelButton(parent)
                self.mainLayout.addWidget(self.buttCancel)
            else:
                self.buttCancel = CancelButton(parent, clicked=cancel)
                self.mainLayout.addWidget(self.buttCancel)

        if clear:
            self.buttClear = XPushButton(parent, text="Clear", ico=Ico.clear, clicked=clear)
            self.mainLayout.addWidget(self.buttClear)

        self.buttSave = SaveButton(parent, text=text, clicked=clicked)
        self.mainLayout.addWidget(self.buttSave)

        self.dbMeta = None
        if self.pk or self.uid:
            self.dbMeta = DbMetaBar(pk=self.pk, uid=self.uid)
            vlay.addWidget(self.dbMeta)


    def set_rec(self, rec):
        self.dbMeta.set_rec(rec)

    def showMessage(self, mess, timeout=None, warn=None, info=None):
        self.statusBar.showMessage(mess, timeout=timeout, warn=warn, info=info)

    def set_reply(self, reply):
        self.statusBar.set_reply(reply)

    def set_busy(self, is_busy):
        self.statusBar.set_busy(is_busy)

    def set_dirty(self, *args):
        self.buttSave.set_dirty()

    def clear_dirty(self):
        self.buttSave.clear_dirty()

    def is_dirty(self):
        return self.buttSave.dirty



class DbMetaBar( QtWidgets.QWidget ):

    def __init__(self, parent=None, uid=None, pk=None):
        super().__init__(parent)

        self.uid = uid
        self.pk = pk

        self.mainLayout = vlayout()
        self.setLayout(self.mainLayout)

        sty = "font-size: 8pt;"
        self.lblUID = XLabel(style=sty)
        self.mainLayout.addWidget(self.lblUID)


        self.lblPK = XLabel(style=sty)
        self.mainLayout.addWidget(self.lblPK)

        self.up_widgets()

    def up_widgets(self):
        self.lblUID.setVisible(self.uid is not None)
        self.lblPK.setVisible(self.pk is not None)

    def set_rec(self, rec):
        if self.uid:
            s = "%s=%s" % (self.uid, rec[self.uid])
            self.lblUID.setText(s)
        if self.pk:
            s = "%s=%s" % (self.pk, rec[self.pk])
            self.lblPK.setText(s)


C_FG = "color: #222222;"
C_BG = "background-color: white;"

C_FG_FOCUS = "" #"color: black;"
#
#C_BG_FOCUS = "background-color: #FFFA93"
C_BG_FOCUS = "border-color: purple"


class XLineEdit( QtWidgets.QLineEdit ):

    sigFocused = pyqtSignal(bool)
    sigMove = pyqtSignal(bool)
    sigDoubleClicked = pyqtSignal()

    def __init__( self, parent=None, show_focus=False, width=None, decimal=None,
                  dirty=True, changed=None, required=None ):
        super().__init__()

        self._show_focus = show_focus

        if width:
            self.setFixedWidth(width)

        if dirty:
            if hasattr(parent, "set_dirty"):
                self.textChanged.connect(parent.set_dirty)
        if changed:
            self.textChanged.connect(changed)

        self.required = required
        self.up_style()

        if decimal is not None:
            self.setValidator(QtGui.QDoubleValidator(0,100000, 2))


    def stripped(self):
        self.setText( self.text().strip() )
        return self.text()

    def s(self):
        return self.stripped()

    def setText(self, txt):
        if txt == None:
            self.setText("")
            return
        if isinstance(txt, float):
            self.setText(str(txt))
            return
        super().setText(txt.strip())

    def set_bold(self, state):
        f = self.font()
        f.setBold(state)
        self.setFont(f)

    def mouseDoubleClickEvent_MAYBE(self, ev):
        ev.ignore()
        self.sigDoubleClicked.emit(ev)

    def up_style(self):
        if self.required:
            s = "background-color: #FEFFC0;"
        else:
            s = "background-color: white;"
        self.setStyleSheet(s)

    def set_required(self, req):
        self.required = req
        self.up_style()

    def focusInEvent(self, ev):
        """Changes style if show_focus """
        if self._show_focus:
            self.setStyleSheet(C_FG_FOCUS + C_BG_FOCUS)
        QtWidgets.QLineEdit.focusInEvent(self, ev)
        self.sigFocused.emit(True)

    def focusOutEvent(self, ev):
        """Changes style if show_focus """
        if self._show_focus:
            self.setStyleSheet(C_FG + C_BG)

        QtWidgets.QLineEdit.focusOutEvent(self, ev)
        self.sigFocused.emit(False)


    def keyPressEvent(self, ev):
        """Clear field with esc, otherwise passthough"""
        if ev.key() == Qt.Key_Escape:
            self.setText("")
            return
        if ev.key() == Qt.Key_Up:
            self.sigMove.emit(False)
            return
        if ev.key() == Qt.Key_Down:
            self.sigMove.emit(True)
            return
        QtWidgets.QLineEdit.keyPressEvent( self, ev )


class XPlainTextEdit(QtWidgets.QPlainTextEdit):

    def __init__(self, max_height=None):
        super().__init__()


        if max_height:
            self.setMaximumHeight(max_height)


    def s(self):
        self.setPlainText(self.toPlainText().strip())
        return self.toPlainText().replace("\r", "")


PAGE_SIZE = 100


class StatusBar(QtWidgets.QWidget):
    """A QWidget with many embedded widgets for StatusBar"""

    sigRefresh = pyqtSignal()

    def __init__(self, parent, refresh=True, status=True,
                 pager=False, mode=True):
        super().__init__()

        if isinstance(parent, bool):
            print("STATUSBar is not correct parent")
            # print Tantrum
            return

        self._lastReply = None

        self._refresh = refresh
        self._status = status

        self.mainLayout = QtWidgets.QHBoxLayout()
        m = 0
        self.mainLayout.setContentsMargins(m, m, m, m)
        self.setLayout(self.mainLayout)

        ##=======================================================================
        ### Pager Widget
        self.pagerWidget = None
        if pager:
            self.pagerWidget = QtWidgets.QWidget()
            self.pagerWidget.setMinimumWidth(300)
            self.mainLayout.addWidget(self.pagerWidget)
            pagerLayout = QtWidgets.QHBoxLayout()
            pagerLayout.setContentsMargins(0, 2, 0, 2)
            pagerLayout.setSpacing(2)
            self.pagerWidget.setLayout(pagerLayout)

            pagerLayout.addWidget(QtWidgets.QLabel("Page:"))

            self.buttonGroupPager = QtWidgets.QButtonGroup(self)
            self.buttonGroupPager.setExclusive(True)

            for i in range(1, 10):
                butt = QtWidgets.QToolButton()
                butt.setText(" %s " % i)
                butt.setStyleSheet("font-weight: bold;")
                butt.setCheckable(True)
                butt.setVisible(False)
                pagerLayout.addWidget(butt)
                self.buttonGroupPager.addButton(butt, i)

            pagerLayout.addStretch(20)
            self.buttonGroupPager.buttonClicked.connect(self.on_page_button)
            # self.connect( self.buttonGroupPager, QtCore.SIGNAL( "buttonClicked(QAbstractButton*)" ), self.on_page_button )

        ##=======================================================================
        ## Status Bar
        self.statusBar = QtWidgets.QStatusBar()
        self.statusBar.setSizeGripEnabled(False)
        self.showMessage(" ")
        self.mainLayout.addWidget(self.statusBar, 1)

        ## Main layout to add
        self.extrasLayout = QtWidgets.QHBoxLayout()
        self.mainLayout.addLayout(self.extrasLayout, 0)

        self.lblCount = QtWidgets.QLabel()
        self.mainLayout.addWidget(self.lblCount)

        ## Status widget setup
        """
        pagerLayout.addWidget(QtWidgets.QLabel("Page "))
        self.lblPage = QtWidgets.QLabel("-")
        self.lblPage.setFrameStyle(QtWidgets.QFrame.Sunken)
        self.lblPage.setFrameShape(QtWidgets.QFrame.Panel)
        pagerLayout.addWidget(self.lblPage)

        pagerLayout.addWidget(QtWidgets.QLabel(" of "))
        self.lblPages = QtWidgets.QLabel("-")
        self.lblPages.setFrameStyle(QtWidgets.QFrame.Sunken)
        self.lblPages.setFrameShape(QtWidgets.QFrame.Panel)
        pagerLayout.addWidget(self.lblPages)
        """

        self.lblStatus = QtWidgets.QLabel()
        self.lblStatus.setFixedWidth(30)
        self.lblStatus.setStyleSheet("background-color: #efefef;")
        self.mainLayout.addWidget(self.lblStatus)

        if mode:
            self.lblMode = XLabel(inset=True)
            self.lblMode.setFixedWidth(50)
            self.lblMode.setStyleSheet("background-color: #efefef;")
            self.mainLayout.addWidget(self.lblMode)
            mody = "NoAttr"
            if hasattr(parent, "mode"):
                mody = parent.mode
            self.lblMode.setText("%s" % mody)

        #== Progress Bar
        progressWidget = QtWidgets.QWidget()
        progressWidget.setFixedWidth(40)
        #progressWidget.setMaximumWidth(200)
        self.mainLayout.addWidget(progressWidget)

        progressLayout = QtWidgets.QHBoxLayout()
        m = 0
        progressLayout.setContentsMargins(m, m, m, m)
        progressWidget.setLayout(progressLayout)

        self.progress = QtWidgets.QProgressBar(self)
        # self.progress.setFixedWidth( 40 )
        self.progress.setFixedHeight(15)
        # self.setContentsMargins( m, m, m, m )
        self.progress.setMinimum(0)
        self.progress.setMaximum(1)
        self.progress.setInvertedAppearance(False)
        self.progress.setTextVisible(False)
        self.progress.setVisible(False)
        # self.connect( self.progress, QtCore.SIGNAL('valueChanged(int)'), self.on_progress_value_changed)
        progressLayout.addWidget(self.progress)


        ################################################################
        ## Refrehsing Button
        self.buttRefresh = None
        self.popMenu = None
        if refresh:
            self.buttRefresh = QtWidgets.QToolButton()
            self.mainLayout.addWidget(self.buttRefresh)

            self.buttRefresh.setIcon(Ico.i(Ico.refresh))
            self.buttRefresh.setAutoRaise(True)
            self.buttRefresh.setIconSize(QtCore.QSize(16, 16))
            self.buttRefresh.setStyleSheet("padding: 0px;")

            self.buttRefresh.clicked.connect(self.on_refresh)
            if isinstance(refresh, bool):
                if hasattr(parent, "on_refresh"):
                    self.buttRefresh.clicked.connect(parent.on_refresh)
            else:
                self.buttRefresh.clicked.connect(self._refresh)


    def show_count(self, total=0, count=0, single="", multi="",):

        s = "%s of %s %s" % (count, total, multi)
        self.lblCount.setText(s)


    def on_refresh(self):
        self.sigRefresh.emit()

    ##==============================================================================
    ## Pager
    ##===============================================================================
    def set_pager(self, items):

        count = len(items)
        if count <= PAGE_SIZE:
            page_count = 1
        else:
            page_count = int(count / PAGE_SIZE)
            if count - (page_count * PAGE_SIZE) > 0:
                page_count = page_count + 1

        for b in self.buttonGroupPager.buttons():

            # no, ok = b.text().toInt()
            # b.setEnabled( no <= page_count )
            page_no = self.buttonGroupPager.id(b)
            b.setVisible(page_no <= page_count)

            start = (PAGE_SIZE * page_no) - PAGE_SIZE
            if page_no < page_count:
                end = start + PAGE_SIZE
            else:
                end = count
            b.setProperty("start", QtCore.QVariant(start))
            b.setProperty("end", QtCore.QVariant(end))
            b.setToolTip("%s - %s" % (start + 1, end + 1))
            if page_no == 1 and count > 0:
                b.setChecked(True)

    def page(self):

        butt = self.buttonGroupPager.checkedButton()

        if butt == None:
            return 0, 0
        start = int(butt.property("start"))
        end = int(butt.property("end"))
        return start, end

    def on_page_button(self, butt):
        if self.pagerWidget == None:
            return
        start = int(butt.property("start").toString())
        end = int(butt.property("end").toString())
        self.emit(QtCore.SIGNAL("page"), start, end)

    ###################################################################
    def showMessage(self, mess, timeout=None, warn=None, info=None):
        if warn:
            self.statusBar.setStyleSheet("color: #990000;")
        elif info:
            self.statusBar.setStyleSheet("color: #3361C2;")
        else:
            self.statusBar.setStyleSheet("color: black;")

        if timeout == None:
            self.statusBar.showMessage(mess)
        else:
            self.statusBar.showMessage(mess, timeout)

    def addWidget(self, widget):
        self.extrasLayout.addWidget(widget)

    def addPermanentWidget(self, widget):
        self.mainLayout.addWidget(widget)

    def insertPermanentWidget(self, idx, widget):
        self.statusBar.insertPermanentWidget(idx, widget)

    def on_menu(self, point):

        self.popMenu.exec_(self.mapToGlobal(point))

    def set_status(self, sta):

        self.lblStatus.setText(sta.status)
        if sta.busy == False:
            self.progress_stop()
            return

        self.progress_start()

    def set_reply(self, reply):
        self.lblStatus.setText(reply.status)
        if reply.busy:
            self.progress_start()
            return

        self.progress_stop()
        self._reply = reply
        if reply.error:
            self.showMessage(reply.error, warn=True)

    def progress_start(self):
        self.progress.setMaximum(0)
        self.progress.setVisible(True)
        if self.buttRefresh:
            self.buttRefresh.setDisabled(True)

    def progress_stop(self):
        self.progress.setVisible(False)
        self.progress.setMaximum(1)
        if self.buttRefresh:
            self.buttRefresh.setDisabled(False)

    def showBusy(self, state):
        if state:
            self.progress_start()
        else:
            self.progress_stop()


class XComboBox( QtWidgets.QComboBox ):

    sigFocus = pyqtSignal(bool)

    def __init__( self, parent=None, show_focus=False, editable=None, dirty=True ):
        super().__init__()

        self._show_focus = show_focus

        if editable:
            self.setEditable(True)

        if dirty:
            assert parent != None
            #self.activated.connect(parent.set_dirty)
            self.currentIndexChanged.connect(parent.set_dirty)
            self.currentTextChanged.connect(parent.set_dirty)

    def stripped(self):
        self.setEditText( self.currentText().strip() )
        return self.currentText()

    def s(self):
        return self.stripped()

    def set_text(self, txt):
        if txt == None:
            self.setEditText("")
            return
        self.setEditText(txt.strip())

    def get_index(self, xid):
        if xid == None or xid == "":
            return None
        idx =  self.findData(str(xid), QtCore.Qt.UserRole, QtCore.Qt.MatchExactly)
        if idx == -1:
            return None
        return idx

    def val(self):
        v = self.get_id()
        if v == None:
            return ""
        return v


    def get_id(self, as_int=True):

        s = self.itemData(self.currentIndex())
        if s == None:
            return None
        if as_int:
            return int(s)
        return s

    def select_id(self, sid):
        idx = self.findData(str(sid))
        if idx != -1:
            self.setCurrentIndex(idx)

    def setItemDisabled(self, idx, state=True):
        #http://qt-project.org/forums/viewthread/27969
        if state:
            self.setItemData(idx, 0, QtCore.Qt.UserRole - 1)
        else:
            self.setItemData(idx, 33, QtCore.Qt.UserRole - 1)

    def focusInEvent(self, ev):
        QtWidgets.QComboBox.focusInEvent(self, ev)
        self.sigFocus.emit(True)
        if self._show_focus:
            if self.isEditable():
                self.setStyleSheet(C_FG_FOCUS + C_BG_FOCUS)
            else:
                self.setStyleSheet(C_FG_FOCUS)

    def focusOutEvent(self, ev):
        if self._show_focus:
            if self.isEditable():
                self.setStyleSheet(C_FG + C_BG)
            else:
                self.setStyleSheet(C_FG)
        QtWidgets.QComboBox.focusOutEvent(self, ev)

    def keyPressEvent(self, ev):

        if self._show_focus and not self.isEditable():
            self.showPopup()
        QtWidgets.QComboBox.keyPressEvent(self, ev)

class XTextEdit( QtWidgets.QTextEdit):

    #sigFocused = pyqtSignal(bool)
    #sigMove = pyqtSignal(bool)

    def __init__( self, parent=None, dirty=None ):
        super().__init__()

        if dirty:
            self.textChanged.connect(parent.set_dirty)

    def stripped(self):
        self.setText( self.toPlainText().strip() )
        return self.toPlainText()

    def s(self):
        return self.stripped()





class XToolBar(QtWidgets.QToolBar):

    def __init__( self, parent=None):
        super().__init__(parent)

        self.setContentsMargins(0, 0, 0, 0 )
        #self.setFixedHeight(60)

    def addStretch(self):

        widget = QtWidgets.QWidget()
        sp = QtWidgets.QSizePolicy()
        sp.setHorizontalPolicy(QtWidgets.QSizePolicy.Expanding)
        widget.setSizePolicy(sp)

        self.addWidget(widget)


class XSplitter(QtWidgets.QSplitter):

    def __init__( self, parent=None, orient=None):
        super().__init__()

        if orient is not None:
            self.setOrientation(orient)

        self._name = None


    def addLayout(self, lay):

        w = QtWidgets.QWidget()
        w.setLayout(lay)
        self.addWidget(w)


    def setup_restore(self, parent, stretch=None, collapse=False):
        self._name = get_object_name(parent)
        assert self._name != None

        for idx in range(0, self.count()):
            self.setCollapsible(idx, collapse)

        if stretch is not None:
            for idx, st in enumerate(stretch):
                self.setStretchFactor(idx, st)

        sizz = G.settings.value(self.settings_key)
        if sizz is not None:
            self.setSizes( [int(s) for s in sizz] )

        self.splitterMoved.connect(self.on_splitter_moved)

    @property
    def settings_key(self):
        return "splitter/%s" % self._name

    def on_splitter_moved(self, x, y):
        G.settings.setValue(self.settings_key, self.sizes())

    def setCollapsibleNone(self):
        for c in range(0, self.count()):
            self.setCollapsible(c, False)

    def setStretchFactors(self, stretch_list):

        for idx, stretch in enumerate(stretch_list):
            self.setStretchFactor(idx, stretch)

class VWidget(QtWidgets.QWidget):

    def __init__(self, parent=None):
        super().__init__()

        self.setLayout(vlayout())

    def addWidget(self, w):
        self.layout().addWidget(w)

    def addStretch(self, x):
        self.layout().addStretch(x)

class HWidget(QtWidgets.QWidget):

    def __init__(self, parent=None):
        super().__init__()

        self.setLayout(hlayout())

    def addWidget(self, w, stretch=None):
        if stretch is not None:
            self.layout().addWidget(w, stretch)
        self.layout().addWidget(w)

    def addStretch(self, x):
        self.layout().addStretch(x)


class XLabel(QtWidgets.QLabel):

    sigClicked = pyqtSignal()
    sigDoubleClicked = pyqtSignal()

    def __init__( self, parent=None, bold=False,
                  style=None, base_style="", align=None,
                  text=None, expanding=True,
                  wrap=False, hover_color=None,
                  inset=None, outset=None,
                  height=None, width=None):
        super().__init__()


        self.hover_color = hover_color

        self.base_style = base_style
        self.style = ""
        if style != None:
            self.style = style
        self.setStyleSheet(self.style)

        if bold:
            f = self.font()
            f.setBold(True)
            self.setFont(f)

        if align is not None:
            self.setAlignment(align)

        if text:
            self.setText(text)

        if height:
            self.setFixedHeight(height)
        if width:
            self.setFixedWidth(width)

        self.setWordWrap(wrap)


        if self.hover_color:
            self.set_border("#dddddd")

        if inset is not None:
            self.setFrameStyle(QtWidgets.QFrame.Panel | QtWidgets.QFrame.Sunken)
        if outset is not None:
            self.setFrameStyle(QtWidgets.QFrame.Panel | QtWidgets.QFrame.Raised)

    def setStyleSheet(self, sty):
        QtWidgets.QLabel.setStyleSheet(self, sty + self.base_style)

    def mouseDoubleClickEvent(self, ev):
        ev.ignore()
        self.sigDoubleClicked.emit()

    def mousePressEvent(self, ev):
        ev.ignore()
        self.sigClicked.emit()

class HeaderBar( QtWidgets.QWidget ):

    def __init__( self, parent, title=None, bg_dark="#dddddd"):
        super().__init__()


        self.fg_color = "#555555"
        self.bg_dark = bg_dark
        self.bg_light = "white"


        #########################################################
        ## Container Layout
        self.containerLayout = hlayout()
        self.setLayout( self.containerLayout )

        #self.width_height = 22 if self.subTitle  else 16

        #self.setFixedHeight(40 if self.subTitle else 30)

        #########################################################
        ## mainWidget Container widget##

        self.lblCTop = XLabel(height=2)
        self.containerLayout.addWidget(self.lblCTop)


        mainWidget = QtWidgets.QWidget()
        self.containerLayout.addWidget( mainWidget, 10 )
        mainWidget.setStyleSheet( "background-color: %s;" %  self.bg_light )

        ### Main Box accross
        mainHBox = hlayout()
        mainWidget.setLayout( mainHBox )


        ### Icon Widget
        self.iconWidget = XLabel(style="background: transparent;")
        #self.iconWidget.setPixmap( self.get_pixmap( self.ico , self.width_height ) )
        #self.iconWidget.setContentsMargins( 10, 0, 0, 0 )
        mainHBox.addWidget( self.iconWidget, 0 )

        ### Vertical Box for Labels
        vLabelBox = vlayout() #margin=[10, 5, 0, 5])
        #vLabelBox.setContentsMargins( 10, 5, 0, 5 )
        mainHBox.addLayout( vLabelBox, 2 )


        ### Main Label
        sty = " padding: 5px; font-size: 14pt; color: %s;" % ( self.fg_color )
        sty += "background-color: qlineargradient(x1: 0, y1: 0, x2: 1, y2: 0, stop: 0 %s, stop: 1 %s);" % (self.bg_light, self.bg_light)

        self.mainLabel = XLabel( text=title, style=sty )
        #self.mainLabel.setStyleSheet(  )
        vLabelBox.addWidget( self.mainLabel )

        ### Sub Label
        #if self.lines == 2:
        #    self.subLabel = QtWidgets.QLabel( self.sub_title )
        #    self.subLabel.setStyleSheet( " font-size: 7em; color: %s;" % self.sub_color )
        #    vLabelBox.addWidget( self.subLabel )




        self.gradientLabel = QtWidgets.QLabel()
        self.gradientLabel.setMinimumWidth( 50 )
        #self.gradientLabel.setMaximumWidth( 400 )
        mainHBox.addWidget( self.gradientLabel,2)
        self.set_gradient(  )

        # if self.right_title:
        #     self.rightLabel = QtWidgets.QLabel( self.right_title )
        #     self.rightLabel.setAlignment(Qt.AlignRight)
        #     self.rightLabel.setStyleSheet( " font-size: 8em; font-weight: bold; padding: 3px; background-color: %s; color: %s" % (self.sub_color, "white") )
        #     mainHBox.addWidget( self.rightLabel )



        self.lblCBottom = XLabel(height=2)
        self.containerLayout.addWidget(self.lblCBottom)


        ######################
        ##Status==================
        www = 80
        if False:
            self.statusWidget = QtWidgets.QWidget()
            #self.statusWidget.setMinimumWidth(80)
            self.statusWidget.setFixedWidth(www)
            outerLayout.addWidget(self.statusWidget, 0)

            self.statusLayout = QtWidgets.QVBoxLayout()
            self.statusLayout.setContentsMargins(0,0,0,0)
            self.statusLayout.setSpacing(0)
            self.statusWidget.setLayout(self.statusLayout)
                ### Gradient albel

            self.statusBar = QtWidgets.QStatusBar(self)
            #self.statusBar.setFixedHeight(15)
            self.statusBar.setFixedWidth(www)
            self.statusBar.showMessage("")
            self.statusBar.setSizeGripEnabled(False)
            self.statusLayout.addWidget(self.statusBar, 1)

            self.progress = QtWidgets.QProgressBar()
            self.progress.setRange(0, 0)
            self.progress.setFixedHeight(15)
            self.progress.setFixedWidth(www)
            #sp = self.progress.sizePolicy()
            #sp.setHorizontalPolicy(QtWidgets.QSizePolicy.Maximum)
            #self.progress.setSizePolicy(sp)
            self.statusLayout.addWidget(self.progress, 1)
            self.progress.hide()

            #self.statusLayout.addStretch(1)

            self.timerBusy = QtCore.QTimer()
            self.timerBusy.setInterval( 300 )
            #self.timmy = 0
            self.connect( self.timerBusy, QtCore.SIGNAL( 'timeout()' ), self.on_busy_timer )
            self.c = 0
            self.twips = ['. ', ' .']

            self.timerReset = QtCore.QTimer()
            self.timerReset.setSingleShot( True )
            self.connect( self.timerReset, QtCore.SIGNAL( 'timeout()' ), self.on_reset_timer )


        self.set_gradient()

    def on_busy_timer( self ):

        #self.timmy = self.timmy + self.timerBusy.interval()
        #self.setText( "%s%s %s" % ("Busy ", self.twips[self.c], self.timmy) )
        try:
            self.statusBar.showMessage( "%s%s" % ( "Busy ", self.twips[self.c] ) )
        except RuntimeError as e:
            print("error.busy", e)
        self.c = self.c + 1
        if self.c == 2:
            self.c = 0


    def on_reset_timer( self ):
        try:
            self.setStyleSheet( "" )
            self.statusBar.showMessage( "error" )
        except RuntimeError as  e:
            print(e, self)

    ###################################################################
    def showMessage( self, mess, timeout=None, warn=None ):
        if timeout == None:
            self.statusBar.showMessage( mess )
        else:
            self.statusBar.showMessage( mess, timeout )
        if warn:
            self.statusBar.setStyleSheet("color: #990000;")
        else:
            self.statusBar.setStyleSheet("")

    def set_gradient( self ):
        style_grad = "background-color: qlineargradient(x1: 0, y1: 0, x2: 1, y2: 0, stop: 0 %s, stop: 1 %s);" % (self.bg_light, self.bg_dark)
        self.gradientLabel.setStyleSheet( style_grad )

    def get_pixmap( self, ico, wh ):

        pixmap = QtWidgets.QPixmap( self.img_dir + ico )
        return pixmap.scaled( wh, wh, QtCore.Qt.IgnoreAspectRatio )

    def setHeaders( self, txt, txt_small=None ):
        self.mainLabel.setText( txt )
        if txt_small and self.lines == 2:
            self.subLabel.setText( txt_small )

    def setTitle( self, txt ):
        self.mainLabel.setText( txt )


    def setSubTitle( self, txt ):
        self.subLabel.setText( txt )

    def setIcon( self, icon, wid_hei=None, is_staff=None, from_attrib=False ):
        #self.iconWidget.setPixmap( self.get_pixmap( icon, wid_hei if wid_hei else self.width_height ) )

        if isinstance(icon, QtWidgets.QIcon):
            self.iconWidget.setPixmap( icon.pixmap( QtCore.QSize( 16, 16 )) )
            return
        if from_attrib:
            self.iconWidget.setPixmap( Ico.get(icon, pixmap=True) )
        else:
            self.iconWidget.setPixmap( Ico.icon(icon, pixmap=True) )


class ToolBarGroup(QtWidgets.QWidget):



    def __init__(self, parent=None, title=None, use_key=False,
                 width=None, hide_labels=False, bg='#C9C9C9',
                 is_group=False, exclusive=True,
                 toggle_icons=False, toggle_callback=None):
        super().__init__()

        if width:
            self.setFixedWidth(width)

        self.icons = [Ico.filter_off, Ico.filter_on]

        self.toggle_icons = toggle_icons
        self.toggle_callback = toggle_callback
        self.hide_labels = hide_labels

        self.buttonGroup = None
        self.is_group = is_group
        self.use_key = use_key

        if self.is_group:
            self.buttonGroup = XButtonGroup()
            self.buttonGroup.setExclusive(exclusive)
            if self.toggle_callback:
                self.buttonGroup.buttonClicked.connect(self.on_button_clicked)

        self.group_var = None
        self.callback = None
        self.icon_size = 12
        self.bg_color = bg

        ## Main Layout
        self.setFixedHeight(50)
        mainLayout = vlayout(margin=0, spacing=0)
        self.setLayout(mainLayout)

        ## Label
        self.lblTitle = QtWidgets.QLabel()
        #bg = "#8F8F8F"  ##eeeeee"
        fg = "#444444"  ##333333"
        lbl_sty = ""
        lbl_sty = "background: %s; " % self.bg_color  # qlineargradient(x1: 0, y1: 0, x2: 1, y2: 0, stop: 0 #fefefe, stop: 1 #CECECE);"
        lbl_sty += " color: %s; font-size: 8pt; padding: 1px;" % fg  # border: 1px outset #cccccc;"
        self.lblTitle.setStyleSheet(lbl_sty)
        self.lblTitle.setFrameStyle(QtWidgets.QFrame.Panel | QtWidgets.QFrame.Raised)

        #self.setFrameStyle(QtWidgets.QFrame.Panel | QtWidgets.QFrame.Raised)
        self.lblTitle.setAlignment(QtCore.Qt.AlignCenter)
        self.lblTitle.setFixedHeight(20)
        mainLayout.addWidget(self.lblTitle)

        ## Toolbar
        #self.toolbar = hlayout()
        #mainLayout.addLayout(self.toolbar)
        self.toolbar = QtWidgets.QToolBar()
        self.toolbar.setContentsMargins(0,0,0,0)
        mainLayout.addWidget(self.toolbar)


        if title:
            self.set_title(title)

    def addSeparator(self):
        self.toolbar.addSeparator()

    def checkedId(self):
        return self.buttonGroup.checkedId()

    def checkedButton(self):
        return self.buttonGroup.checkedButton()

    def button(self, key):
        for b in self.buttonGroup.buttons():
            if b.property(PROP_KEY) == key:
                return b
        return None

    def checkedKey(self):
        return self.buttonGroup.checkedButton().property(PROP_KEY)

    def setCheckedKey(self, key):
        for butt in self.buttonGroup.buttons():
            if butt.property(PROP_KEY) == key:
                butt.setChecked(True)


    def set_title(self, title):
        self.lblTitle.setText("%s" % title)

    def addWidget(self, widget):
        self.toolbar.addWidget(widget)
        return widget

    def addAction(self, act):
        self.toolbar.addAction(act)

    def addButton(self, ico=None, text=None, clicked=None, tooltip=None,
                  key=None, id=None, checkable=False, checked=None,
                  bold=False, disabled=None, width=None):

        butt = XToolButton()

        if key is not None:
            butt.setProperty("key", key)

        if self.is_group:
            if id is not None:
                self.buttonGroup.addButton(butt, id)
            else:
                self.buttonGroup.addButton(butt)
        if self.hide_labels == False:
            if text != None:
                butt.setText(text)

        if text == None:
            butt.setToolButtonStyle(QtCore.Qt.ToolButtonIconOnly)
        elif text is not None and ico is not None:
            butt.setToolButtonStyle(QtCore.Qt.ToolButtonTextBesideIcon)
        elif not self.toggle_icons:
            butt.setToolButtonStyle(QtCore.Qt.ToolButtonTextOnly)
        else:
            butt.setToolButtonStyle(QtCore.Qt.ToolButtonTextBesideIcon)

        if tooltip:
            butt.setToolTip(tooltip)
        if disabled is not None:
            butt.setDisabled(disabled)

        if self.toggle_icons:
            butt.setIconSize(QtCore.QSize(10, 10))
            butt.setIcon(Ico.icon(self.icon_off))

        if ico is not None:

            butt.setIcon(Ico.icon(ico))
            #butt.setIconSize(QtCore.QSize(10, 10))

        butt.setCheckable(checkable)
        if checked != None:
            butt.setChecked(checked)


        butt.setProperty(PROP_KEY, key)

        if clicked:
            butt.clicked.connect(clicked)
        if bold:
            self.set_bold(butt)
        if width:
            butt.setFixedWidth(width)

        self.on_button_clicked(block=True)

        self.toolbar.addWidget(butt)
        return butt

    def set_bold(self, w):
        f = w.font()
        f.setBold(True)
        w.setFont(f)

    def on_button_clicked(self, butt=None, block=False):
        if self.is_group:
            for b in self.buttonGroup.buttons():
                if self.toggle_icons:
                    b.setIcon( Ico.icon(self.icons[1] if b.isChecked() else self.icons[0]) )

                if block == False and b.isChecked():
                    if self.toggle_callback:
                        if self.use_key:
                            self.toggle_callback(self.buttonGroup.checkedButton().property(PROP_KEY))
                        else:
                            self.toggle_callback(self.buttonGroup.id(b))

    def get_id(self):
        id = self.buttonGroup.checkedId()
        if id == -1:
            return None
        return id



class GroupHBox(QtWidgets.QGroupBox):


    def __init__(self, parent=None, spacing=5, margin=10, title=None):
        super().__init__()


        self.xlayout = hlayout(spacing=spacing, margin=margin)
        self.setLayout(self.xlayout)


    def addWidget(self, widget, stretch=0):
        self.xlayout.addWidget(widget, stretch)

    def addLayout(self, widget, stretch=0):
        self.xlayout.addLayout(widget, stretch)

    def addStretch(self, stretch):
        self.xlayout.addStretch(stretch)



class GroupVBox(QtWidgets.QGroupBox):


    def __init__(self, parent=None, bold=False, spacing=5, margin=10, title=None):
        super().__init__(parent)

        self.xlayout = vlayout(spacing=spacing, margin=margin)
        self.setLayout(self.xlayout)

        if title:
            self.setTitle(title)

    def addLabel(self, txt):
        lbl = QtWidgets.QLabel()
        lbl.setText(txt)
        lbl.setStyleSheet("font-family: monospace; font-size: 8pt; color: black;  padding: 1px;")
        self.xlayout.addWidget(lbl)
        return lbl


    def addWidget(self, widget, stretch=0):
        self.xlayout.addWidget(widget, stretch)
        return widget

    def addLayout(self, lay, stretch=0):
        self.xlayout.addLayout(lay, stretch)
        return lay

    def addSpacing(self, s):
        self.xlayout.addSpacing( s)

    def addStretch(self, stretch):
        self.xlayout.addStretch(stretch)


    def setSpacing(self, x):
        self.xlayout.setSpacing(x)


class GroupGridBox(QtWidgets.QGroupBox):

    def __init__(self, parent=None, spacing=5, margin=10, title=None):
        super().__init__()


        self.grid = QtWidgets.QGridLayout(spacing=spacing, margin=margin)
        self.setLayout(self.grid)


class XSpinBox(QtWidgets.QSpinBox):

    def __init__( self , changed=None):
        super().__init__()


        if changed:
            self.valueChanged.connect(changed)

class XTabBar(QtWidgets.QTabBar):

    def __init__( self, parent=None ):
        super().__init__()


    def addTab(self, ico=None,  text=None, closable=True, data=None):

        newIdx = super().addTab(Ico.i(ico), text)

        self.setTabData(newIdx, data)
        #if not closable:
        #    self.tabButton(nidx, QtWidgets.QTabBar.RightSide).deleteLater()
        #    self.setTabButton(nidx, QtWidgets.QTabBar.RightSide, None)
        if not closable:
            self.tabButton(newIdx, QtWidgets.QTabBar.RightSide).resize(0 ,0)
        return newIdx

class XCheckBox( QtWidgets.QCheckBox ):

    #sigFocused = pyqtSignal(bool)
    #sigMove = pyqtSignal(bool)

    def __init__( self, parent=None, text=None, checked=None,
                  toggled=None, dirty=True ):
        super().__init__(parent )


        if text:
            self.setText(text)

        if toggled:
            self.toggled.connect(toggled)

        if dirty:
            assert parent is not None
            self.toggled.connect(parent.set_dirty)

        if checked != None:
            assert isinstance(checked, bool)
            self.setChecked(checked)


    def setChecked(self, sta):
        super().setChecked(bool(sta))

class InfoLabel(QtWidgets.QLabel):

    INFO = "Info"
    WARNING = "Warning"
    WIP = "WorkInProgress"

    def __init__(self, type=INFO, text=None):
        super().__init__()

        self.setText(text)
        self.set_stylesheet()

    def set_stylesheet(self):
        s = "padding: 8px; border-radius: 5px; background-color: blue;"
        self.setStyleSheet(s)


class XDialog(QtWidgets.QDialog):

    def __init__(self, parent=None):
        super().__init__(parent)


    def keyPressEvent(self, ev):
        if ev.key() == Qt.Key_Enter or ev.key() == Qt.Key_Return:
            return
        super().keyPressEvent(ev)




class XMenu(QtWidgets.QMenu):

    def __init__(self):
        super().__init__()


    def add(self, ico=None, text=None, clicked=None):

        act = QtWidgets.QAction()
        if text:
            act.setText(text)
        if ico:
            act.setIcon(Ico.i(ico))
        if clicked:
            act.triggered.connect(clicked)
        self.addAction(act)
        return act



class XSlider(QtWidgets.QSlider):

    def __init__( self, parent=None, orient=None, changed=None):
        super().__init__()

        if orient is not None:
            self.setOrientation(orient)

        if changed:
            self.valueChanged.connect(changed)

    def setValue(self, v, block=False):
        if block:
            super().blockSignals(True)
        super().setValue(v)
        if block:
            super().blockSignals(False)


class Line(QtWidgets.QLabel):
    def __init__( self, parent=None, color="#efefef", size=DEFAULT_LINE_SIZE):
        super().__init__()

        self.setFixedHeight(size)
        sty = "background-color: %s;" % color
        self.setStyleSheet(sty)

class IconLabel(QtWidgets.QLabel):

    def __init__( self, parent=None, ico=None):
        super().__init__( parent)

        self.setContentsMargins(5,0,0,0)

        icon = Ico.icon(ico)
        self.setPixmap( icon.pixmap(QtCore.QSize( 16, 16 )) )


class PopupHeader(QtWidgets.QWidget):

    def __init__(self, parent=None, ico=None, min_width=200, text=None):
        super().__init__(parent)


        mainLayout = hlayout()
        self.setLayout(mainLayout)

        self.setMinimumWidth(min_width)

        self.lblIco = QtWidgets.QLabel()
        self.lblIco.setFixedWidth(32)
        self.lblIco.setContentsMargins(7, 0, 0, 0)
        mainLayout.addWidget(self.lblIco)

        self.lblLabel = QtWidgets.QLabel()
        mainLayout.addWidget(self.lblLabel)

        sty = "background-color: #efefef;  padding: 4px 4px 4px 0; border-bottom: 1px outset #999999; font-weight: bold;"
        self.lblLabel.setStyleSheet(sty)

        if text:
            self.setText(text)
        if ico:
            self.setIco(ico)


    def setIcon(self, icon):
        self.lblIco.setPixmap(icon.pixmap(QtCore.QSize(16, 16)))

    def setIco(self, ico=None):
        icon = Ico.icon(ico)
        self.lblIco.setPixmap(icon.pixmap(QtCore.QSize(16, 16)))

    def setText(self, txt):
        self.lblLabel.setText(txt)
