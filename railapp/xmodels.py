
from Qt import Qt, QtWidgets, QtGui, QtCore, pyqtSignal

from img import Ico


ID_ROLE = QtCore.Qt.UserRole + 11
#TS_ROLE = QtCore.Qt.UserRole + 12
SORT_ROLE = QtCore.Qt.UserRole + 13
DATE_ROLE = QtCore.Qt.UserRole + 14
VALUE_ROLE = QtCore.Qt.UserRole + 15

DEFAULT_BG = "#E1FFBC"

class ActiveFlag:
    """Flag definition obj"""
    def __init__(self, key, lbl, ico=None):

        self.lbl = lbl
        """Label"""
        self.ico = ico
        """Icon"""
        self.key = key
        """Filter key"""

class ACTIVE_FLAGS:
    """Constants for the active filter (accounts, contacts etc)"""

    active = ActiveFlag(1, "Active") #, Ico.FilterActive)
    all = ActiveFlag(2, "All") #, Ico.FilterAll)
    inactive = ActiveFlag(0, "Inactive")#, Ico.FilterInActive)

    @staticmethod
    def options():
        return [ACTIVE_FLAGS.active, ACTIVE_FLAGS.all, ACTIVE_FLAGS.inactive]





class XColDef:
    """Column Definition used by :class:`~widgets.xmodels.XColDef.col_defs`"""
    def __init__(self, cidx=None, fld=None, label=None,
                 align=None,  bold=None, bg=None,
                 yn=None, tick=None, is_cash=None,
                 resize=None, width=None,
                 is_date=None, is_time=None, is_count=None, ):

        self.cidx = cidx
        """index of this col"""

        self.fld = fld
        """field_name of did"""

        self.label = label
        """Column header label"""

        self.align = align
        """Text alignment"""

        self.is_date = is_date
        self.is_time = is_time
        self.is_date_time = None
        """Column is a date"""

        self.is_count = is_count
        """Column is a count ?"""

        self.bold = bold
        """Make bold"""

        self.yn = yn
        """Returns Yes or No if true"""

        self.tick = tick
        """Return a tick UTF.dingbat if True and aligns center"""

        self.resize = resize
        """Whether to resize col to contents"""

        self.width = width
        """Width"""

        self.is_cash = is_cash
        """Cash item (aligns right)"""

        self.bg = None


    def __repr__(self):
        di = " date" if self.is_date else ""
        return "<C:%s:%s%s>" % (self.cidx, self.fld, di)


class XDictModel(QtCore.QAbstractTableModel):

    sigReply = pyqtSignal(object)
    sigLoaded = pyqtSignal(int)

    def __init__( self, parent=None, mode=None):
        super().__init__()

        self.initial_load = False

        self.mode = mode
        """mode flag for model"""


        self.col_defs = []
        """List of :class:`~widgets.xmodels.XColDef` column definitions"""

        self.recs = None
        """List of dicts as data"""

        self.pk = None
        """Primary key for table"""

        self._head_lines = 2
        """No of lines in header"""

        ## TODO paging
        #self.rows_loaded = XAbstractItemModel.PAGE_SIZE


    def init_col_defs(self, colsClass, head_lines=None, pk=None):
        """Takes a COL Class, and creates the fields

            class C:
              full_name = 0
              email = 1
              sex = 2


        """
        self.pk = pk

        if head_lines:
            self._head_lines = head_lines

        # lookup dict of col vs fld name ie opposite
        col_dict = {}
        for fld, col in vars(colsClass).items():
            if fld[0:2] != "__":
                if col in col_dict:
                    print("ERR: dupe col no", fld, "=", col_dict[fld])
                    assert True == True
                else:
                    col_dict[col] = fld

        # check dict is valid ie its in sequence, cols missing etc
        for idx, cidx in enumerate(sorted(col_dict.keys())):
            if idx != cidx:
                print("ERRR: oops, non sequential cols vs idx")
                assert True == True

        ## assemble col defs
        for cidx in sorted(col_dict.keys()):

            fld = col_dict[cidx]

            #parts = fld.replace("_", " ").title().split(" ", 2)
            lbl = self._mk_label(fld.replace("_", " ").title())
            # if len(parts) == 1:
            #     lbl = "\n" + parts[0]
            # else:
            #     lbl = "\n".join(parts)

            is_date = True if "date" in fld else None

            colDef = XColDef(fld=fld, cidx=cidx, label=lbl, is_date=is_date)
            self.col_defs.append(colDef)

    def _mk_label(self, s):
        parts = s.split(" ", 2)
        if len(parts) == 1:
            return "\n" + parts[0]
        return "\n".join(parts)

    def set_header(self, cidx,  label=None, is_time=None,
                    align=None, bold=None, is_cash=None, bg=None,
                    yn=None, tick=None, width=None, resize=None,
                    is_date=None, is_date_time=None,
                    is_count=None, ico=None):

        colDef = self.col_defs[cidx]

        #if label != None:
        #    colDef.label = self._mk_label(label)

        if label != None:
            colDef.label = label


        if is_count != None:
            colDef.is_count = is_count
            colDef.align = Qt.AlignCenter

        if yn != None:
            colDef.yn = yn
            colDef.align = Qt.AlignCenter
        if tick != None:
            colDef.tick = tick
            colDef.align = Qt.AlignCenter

        if is_date is not None:
            colDef.is_date = is_date
        if is_time is not None:
            colDef.is_time = is_time
        if is_date_time is not None:
            colDef.is_date_time = is_date_time

        if ico is not None:
            colDef.ico = ico

        if align is not None:
            colDef.align = align
        if width is not None:
            colDef.width = width
        if resize is not None:
            colDef.resize = resize

        if is_cash:
            colDef.is_cash = True
            colDef.align = Qt.AlignRight

        if bg is not None:
            colDef.bg = bg


        colDef.bold = bold


    def index(self, row, col, parent=None):
        return self.createIndex(row, col)

    def parent(self, pidx=None):
        return QtCore.QModelIndex()

    def columnCount(self, midx=None):
        return len(self.col_defs)

    def rowCount(self, midx=None):
        if self.recs == None:
            return 0
        return len(self.recs)

    def get_dic_from_midx(self, midxOb):
        midx = midxOb[0] if isinstance(midxOb, list) else midxOb
        return self.recs[midx.row()]

    def get_dic_from_ridx(self, ridx):
        return self.recs[ridx]

    def get_ridx(self, val, field=None):
        if field == None:
            assert self.pk != None
            field = self.pk
        for idx, rec in enumerate(self.recs):
            if rec[field] == val:
                return idx
        return None

    def get_midx(self, val, field=None):
        ridx = self.get_ridx(val, field=field)
        if ridx is None:
            return None
        return self.index(ridx, 0)

    def headerData(self, section, orient, role):
        if role == Qt.DisplayRole:
            return self.col_defs[section].label

        if role == Qt.TextAlignmentRole:
            return self.col_defs[section].align

    def data(self, midx, role):

        hd: XColDef = self.col_defs[midx.column()]

        if role == DATE_ROLE:
            v = self.recs[midx.row()].get(hd.fld)
            return "" if v is None else v

        if role == Qt.DisplayRole:

            v = self.recs[midx.row()][hd.fld]

            if hd.is_date_time:
                return "%s %s" % (xdate.dmy(v), xdate.hm12(v))

            elif hd.is_date:
                return xdate.dmy(v)

            elif hd.is_time:
                return xdate.hm12(v)

            elif hd.is_cash:
                if v == None:
                    return "-"
                return '{0:.2f}'.format(v)

            elif hd.is_count:
                if v == None:
                    return "-"
                elif v > 0:
                    return v
                return "-"

            elif hd.yn != None:
                return "Y" if v else "-"

            elif hd.tick:
                 return u'\u2714' if v else ""

            return v

        if role == Qt.BackgroundRole:
            if hd.yn:#
                v = self.recs[midx.row()].get(hd.fld)
                color = hd.bg if hd.bg else DEFAULT_BG
                return QtGui.QColor(color) if v else None

        if role == Qt.TextAlignmentRole:
            return hd.align

        # if role == Qt.DecorationRole:
        #     if hd.ico:
        #         return deadIco.i(hd.ico)

        if role == Qt.FontRole:
            if hd.bold:
                return self._mk_font(bold=True)

    def _mk_font(self, bold=True):
        f = QtGui.QFont()
        f.setBold(bold)
        return f

    def set_aligns(self, align, *args ):
        for cidx in args:
            self.col_defs[cidx].align = align

    def clear(self):
        self.recs = []
        self.modelReset.emit()

    def set_recs(self, recs):
        self.recs = recs
        self.modelReset.emit()
        self.sigLoaded.emit(len(self.recs))
        self.initial_load = True

    def upsert(self, urec):

        assert self.pk != None
        pk_id = urec[self.pk]

        for ridx, rec in enumerate(self.recs):
            if pk_id == rec[self.pk]:
                self.recs[ridx] = urec
                midx = self.index(ridx, 0)
                self.dataChanged.emit(midx, self.index(ridx, self.columnCount()))
                return midx

        ## so its new
        self.recs.append(urec)
        #ridx = len(self.recs) - 1
        #midx = self.index(ridx, 0)
        #print("ere", ridx, midx, midx.isValid())
        #dself.dataChanged.emit(midx, self.index(ridx, self.columnCount()))
        self.layoutChanged.emit()
        #self.rowsInserted.emit(midx, ridx, ridx)
        self.sigLoaded.emit(len(self.recs))
        return


