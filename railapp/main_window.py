# -*- coding: utf-8 -*-
# @author: Peter Morgan <pete@daffodil.uk.com>

import os
import sys
import datetime


from Qt import Qt, QtGui, QtCore, QtWidgets

from railapp import ORG_NAME, DOMAIN
import xwidgets

import folders_widget
import kml_view
import stations_widget, stations_db
import station_widget
import rail_widgets

class MainWindow(QtWidgets.QMainWindow):
    """Inherited by all other main windows"""


    def on_after(self):
        # print("on_after - TO IMPLEMENT", self)
        pass
        #self.job_wizard(rec={})

        self.setGeometry( 10, 10, 1200, 800 )
        self.move( 1600, 20 )
        self.setWindowState( Qt.WindowMaximized )

        self.wwwFolders.scan_dirs()

    @staticmethod
    def show_splash():

        splashImage = QtWidgets.QPixmap("../images/corp/splash.png")
        splashScreen = QtWidgets.QSplashScreen(splashImage)
        splashScreen.showMessage("  Loading . . . %s" % G.settings.version())
        splashScreen.show()
        return splashScreen


    def __init__(self, parent=None):
        super().__init__(parent)

        ##==========================================
        ## Global Models


        #QtGui.QFontDatabase.addApplicationFont(os.path.join(G.STATIC_PATH, "fonts", "RobotoCondensed-Bold.ttf"))


        QtWidgets.QApplication.setOrganizationName(ORG_NAME)
        QtWidgets.QApplication.setOrganizationDomain(DOMAIN)
        QtWidgets.QApplication.setApplicationName(ORG_NAME)
        QtWidgets.QApplication.setApplicationVersion("2020+")


        # ========================================================================
        # Main Window
        self.setWindowTitle(ORG_NAME)
        #self.setWindowIcon(Ico.logo())

        ##=======================
        ###  Meniws

        ##=== File Menu
        self.menuFile = self.menuBar().addMenu("File")


        self.menuFile.addSeparator()

        self.menuFile.addAction("Upgrade", self.on_upgrade)

        self.menuFile.addSeparator()

        #self.menuFile.addAction(Ico.i(Ico.clear), "Clear local cache", self.clear_cache)
        #self.menuFile.addAction(Ico.i(Ico.clear), "Reload local cache", self.reload_cache)
        #self.menuFile.addSeparator()

        #self.actionQuit = self.menuFile.addAction(Ico.i(Ico.logout), "Quit", self.on_quit)
        #self.actionQuit.setIconVisibleInMenu(True)



        # ====================================================================
        ## Settings Menu
        self.menuSettings = self.menuBar().addMenu("Settings")



        self.menuSettings.addSeparator()

        ## ====
        # set Style Menu
        self.menuStyle = self.menuSettings.addMenu("Style")
        self.menuStyle.triggered.connect(self.on_style)
        actGroup = QtWidgets.QActionGroup(self)
        for i in QtWidgets.QStyleFactory.keys():
            act = self.menuStyle.addAction(i)
            act.setCheckable(True)
            if QtWidgets.QApplication.style().objectName() == i.lower():
                act.setChecked(True)
            actGroup.addAction(act)




        # ================================
        ### Help Menu
        self.menuHelp = self.menuBar().addMenu("Help")


        self.menuHelp.addSeparator()
        self.menuHelp.addAction("About Qt", self.on_about_qt)


        #self.mainLayout = xwidgets.vlayout()
        #self.setLayout(self.mainLayout)

        self.tabWidget = QtWidgets.QTabWidget()
        #self.mainLayout.addWidget(self.tabWidget, 100)
        self.setCentralWidget(self.tabWidget)



        self.splittCols = xwidgets.XSplitter()
        self.tabWidget.addTab(self.splittCols, "Import")


        ### WWW folders
        self.wwwFolders = folders_widget.FoldersWidget()
        self.splittCols.addWidget(self.wwwFolders)


        ### KML
        self.kmlView = kml_view.KMLViewWidget()
        self.splittCols.addWidget(self.kmlView)

        ## STations =======
        self.splitStations = xwidgets.XSplitter(orient=Qt.Vertical)
        self.splittCols.addWidget(self.splitStations)

        self.stationsWidget = stations_widget.StationsWidget()
        self.splitStations.addWidget(self.stationsWidget)

        self.stationsDb = stations_db.StationsDBWidget()
        self.splitStations.addWidget(self.stationsDb)

        ### Station
        self.splitStation = xwidgets.VWidget()
        self.splittCols.addWidget(self.splitStation)


        self.stationWidget = station_widget.StationWidget()
        self.splitStation.addWidget(self.stationWidget)

        self.wikiWidget = rail_widgets.Wikipedia()
        self.splitStation.addWidget(self.wikiWidget)

        ## Connect
        self.wwwFolders.sigFolder.connect(self.kmlView.load_kml)
        self.kmlView.sigSaved.connect(self.on_station_written)
        self.stationsWidget.sigFolder.connect(self.on_station_clicked)




        QtCore.QTimer.singleShot(300, self.on_after)

    def on_station_written(self, folder):
        self.stationsWidget.scan_dirs()
        self.stationWidget.load_station(folder)


    def on_style( self, action ):
        QtWidgets.QApplication.setStyle( QtWidgets.QStyleFactory.create( action.text() ) )

    def on_about_qt( self ):
        QtWidgets.QMessageBox.aboutQt( self )


    def on_upgrade(self):
        pass

    def clear_cache(self):
        #G.Cache.clear_all()
        self.flash("Cache cleared", 2000, info=True)

    def reload_cache(self):
        #G.cache.reload()
        self.flash("Reloading", 2000, info=True)


    def show_help(self, page=None):
        self.helpDialog.show_page(page)


    def on_station_clicked(self, sta):
        self.stationWidget.load_station(sta)
        self.wikiWidget.txtSearch.setText(sta)


