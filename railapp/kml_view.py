# -*- coding: utf-8 -*-
# @author: Peter Morgan <pete@daffodil.uk.com>

import os
import subprocess
import json
import yaml
import geojson


from Qt import QtGui, QtCore, Qt, QtWidgets, pyqtSignal

import xwidgets
import xtree
from img import Ico
import ut

from railapp import PROJECT_PATH, TEMP_PATH
import railib

def paths(folder):
    fp = os.path.join(PROJECT_PATH, folder)

    kml_path = "%s/%s.kml" % (fp, folder)

    return fp, kml_path




class C:
    node = 0
    name = 1

class KMLViewWidget( QtWidgets.QWidget ):

    sigSaved = pyqtSignal(str)

    def __init__( self, parent=None):
        super().__init__()

        self.data = {}
        self.folder = None


        self.mainLayout = xwidgets.vlayout()
        self.setLayout( self.mainLayout )

        self.mainTitle = xwidgets.XLabel()
        self.mainLayout.addWidget(self.mainTitle)

        self.toolbar = xwidgets.XToolBar()
        self.mainLayout.addWidget(self.toolbar)

        #self.actUpdateIndex = self.toolbar.addAction("Import KML", self.write_lines_index)
        self.actUpdateIndex = self.toolbar.addAction("Line.Info", self.write_line_info)

        self.tree = QtWidgets.QTreeWidget()
        self.mainLayout.addWidget(self.tree)
        self.tree.setSortingEnabled(False)

        hi = self.tree.headerItem()
        hi.setText(C.node, "")
        hi.setText(C.name, "Folder")

        #self.lst = []
        #self.statusBar = xwidgets.StatusBar(self, refresh=self.scan_dirs)
        #self.mainLayout.addWidget(self.statusBar)
        self.buttGroup = QtWidgets.QButtonGroup()
        self.buttGroup.setExclusive(False)
        self.buttGroup.buttonClicked.connect(self.on_write_station_butt)


    def write_line_info(self):

        #line_meta, err = railib.parse_content_file( PROJECT_PATH, self.folder )
        #print("META+++", line_meta)
        railib.write_line_info(self.folder)
        return

        idx_file = self.full_path() + "/data.json"

        statts = []
        for sta in self.data['stations']:
            print(sta)
            sta['path'] = "/station/%s/" % (sta['name'].lower())
            statts.append(sta)

        data = dict(
            stations = statts,
            title = line_meta["title"],
            path = "/%s/" % self.folder
        )

        ut.write_json_file(idx_file, data )


        nearby = []
        stats = self.data['stations']
        lenny = len(stats)
        for idx, sta in enumerate(stats):
            if idx > 0:
                #xx = stats[-2:idx]
                for xx in stats[-2:idx -1]:

                    nearby.append(dict(name=xx['name'], path=xx["path"]))

            if idx < lenny:
                #nearby.extend(stats[idx:2])
                for xx in stats[idx+1:2]:
                    #print(xx)
                    nearby.append(dict(name=xx['name'], path=xx["path"]))

            self.write_station(sta['name'].lower(), sta, nearby, line_meta)

    def write_station(self, sta_folder, idx, nearby=[]):

            stations = self.data['stations']
            staMeta = stations[idx]

            outfile_pth = os.path.join(PROJECT_PATH, "station", sta_folder, "index.md")
            #print("out==", outfile_pth)


            staMeta['template'] = "station.html"

            staMeta['info']['type'] ="station"
            staMeta['info']['status'] = "closed"

            staMeta['info']['lines'] = [dict(
                    path="/%s/" % self.folder,
                    title=self.line_meta["title"]
                )]

            disused, err = railib.get_disused(sta_folder)
            #print("DISSSSSSSSS=", disused)
            if err is None:
                staMeta['info']['disused'] = disused

            nearby = []
            lenny = len(stations)


            if idx > 0:
                #xx = stats[-2:idx]
                print("TTTTTTTTTT=", idx, lenny)

                for xx in stations[idx-2:idx]:
                    print(xx)

                    nearby.append(dict(title=xx['title'], path="/station/" + ut.slug(xx["title"])))

            if idx < lenny:
                #nearby.extend(stats[idx:2])
                print("ADTER", idx, lenny)
                for xx in stations[idx+1:idx+3]:
                    #print(xx)
                    nearby.append(dict(title=xx['title'], path="/station/" + ut.slug(xx["title"])))

            staMeta['info']['nearby'] = nearby


            yss = yaml.dump(staMeta)

            s = "---\n"
            s += yss
            s += "\n---\n\n"


            #s += "Foo bar"

            ut.write_file(outfile_pth, s)

            self.sigSaved.emit(sta_folder)


    def full_path(self):
        return os.path.join(PROJECT_PATH, self.folder)

    def load_kml(self, folder):

        self.folder = folder
        self.tree.clear()

        self.mainTitle.setText(folder)

        self.data, err = railib.kml_to_geojson(folder)

        self.line_meta, err = railib.read_frontmatter( PROJECT_PATH, self.folder )



        #print(self.cache['stations'])
        for idx, sta in enumerate(self.data['stations']):
            #if os.path.isdir(pth):
            #kml = os.path.join(pth, "%s.kml" % sdir)

            ico = Ico.kmlf if True else Ico.folder
            item = xtree.XTreeWidgetItem()
            item.setText(C.name, sta['title'])
            item.set_ico(C.name, ico)
            self.tree.addTopLevelItem(item)

            butt = xwidgets.XPushButton(text="Add")
            butt.setProperty("title", sta['title'])
            self.tree.setItemWidget(item, C.node, butt)
            self.buttGroup.addButton(butt, idx)

    def on_write_station_butt(self, butt):
        #title = butt.property("title")
        idx = self.buttGroup.id(butt)
        #print(butt.text(), name)

        staMeta = self.data['stations'][idx]



        folder = staMeta['title'].lower().strip().replace(" ", "_")

        sta_path = os.path.join(railib.STATIONS_PATH, folder)
        if not os.path.exists(sta_path):
            os.mkdir(sta_path)

            text = QtWidgets.QInputDialog.getText(self, 'New Station', 'enter dir:', QtWidgets.QLineEdit.Normal, folder)
            if text[1]:
                folder = text[0]
                #print(station)
                #folder = os.path.join(railib.STATIONS_PATH, folder)
        self.write_station(folder, idx)





    def do_test(self):
        src = os.path.join(TEMP_PATH, "anglesey-central.geojson")
        dest =  os.path.join(PROJECT_PATH, "anglesey-central", "data.geojson")

        railib.clean_kml_dump(src, dest)
        return
        lst = []
        with open(os.path.join(PROJECT_PATH, "anglesey-central", "anglesey-central.geojson_1")) as f:
            data = geojson.load(f)
            for feat in data['features']:

                if feat['geometry']['type'] == "Point":
                    lst.append(feat)

                elif feat['geometry']['type'] == "LineString" and not "FLY" in feat['properties']['name'].upper():
                    lst.append(feat)


        data = lst
        pp = os.path.join(PROJECT_PATH, "anglesey-central", "anglesey-central.2.geojson")
        print(pp)
        ut.write_json_file(pp, data)


