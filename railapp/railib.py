

import os
import subprocess
import json
import geojson

import requests
import frontmatter
import bs4

from railapp import TEMP_PATH, PROJECT_PATH, ROOT_PATH, PROJECT_ROOT
import ut

DISUSED_URL = "http://disused-stations.org.uk"
DISUSED_CACHE_FILE = os.path.join(TEMP_PATH, "disused-stations.json")
STATIONS_PATH = os.path.join(PROJECT_PATH, "station")

DATA_PATH = os.path.join(PROJECT_ROOT, "_data")

WWW_ROOT = "http://127.0.0.1:8000"
#WWW_ROOT = "https://welsh-railways.gitlab.io"

disusedCache = None

def wiki_search(search):

    url = "https://en.wikipedia.org/w/api.php"

    params = dict(
        action = "opensearch",
        search = search,  # search query
        limit = 10,  # return only the first result
        namespace = 0,  # search only articles, ignoring Talk, Mediawiki, etc.
        format = "json"  # jsonfm prints the JSON in HTML for debugging.
    )

    r = requests.get(url, params=params)

    if r.status_code != 200:
        return None, "Error %s" % r.status_code

    lst = []
    name, titles, summary, links =  r.json()
    print("=====", name, titles, summary, links)
    for idx, tit in enumerate(titles):
        print(">>", tit, links[idx])
        lst.append(dict(title=tit, url=links[idx]))

    return lst, None


def clean_kml_dump(src, dest):
    lst = []
    with open(src) as f:
        data = geojson.load(f)
        for feat in data['features']:

            if feat['geometry']['type'] == "Point":
                lst.append(feat)

            elif feat['geometry']['type'] == "LineString" and not "FLY" in feat['properties']['name'].upper():
                lst.append(feat)

    #dic = dict(name="title", type="FeatureCollection", features=lst)
    print(lst)
    ut.write_json_file(dest, lst)

def dssssssdwrite_stations_index():

    disu_list, err = ut.read_json_file(DISUSED_CACHE)
    lookup = {}
    for rec in disu_list:
        if rec['name'] is not None:
            lookup[rec['name'].lower()] = rec
    #print(lookup.keys())
    toc = []
    idxx = {}

    for sdir in sorted(os.listdir(STATIONS_PATH)):
        pth = os.path.join(STATIONS_PATH, sdir, "index.md")
        print(pth)
        if not os.path.exists(pth):
            continue
        with open(pth) as f:
            data = frontmatter.load(f)
            #print("data=", data['meta'])


            n = data['meta']['name']

            aa = n[0]
            if not aa in idxx:
                idxx[aa] = []

            #lst.append(dict(path=sdir, name=n))
            uu = "- [%s](/station/%s/)" %(n, sdir)
            toc.append(uu)
            idxx[aa].append(uu)

    s = "Stations Index\n================\n\n"

    #s += "\n".join(toc)
    for aa, items in idxx.items():
        s += "## %s\n\n" % aa.upper()

        s += "\n".join(items)
        s += "\n\n"

    s += "\n\n"

    stations_md = os.path.join(PROJECT_PATH, "stations.md")
    ut.write_file(stations_md, s)


def write_line_info(folder):

    #line_meta, err = railib.parse_content_file( PROJECT_PATH, self.folder )
    #print("META+++", line_meta)


    target_file = os.path.join(PROJECT_PATH, folder, "data.json")

    line_meta, err = read_frontmatter(PROJECT_PATH, folder)
    #print(line_meta['info'])
    data = dict(
        stations = read_stations(True),
        title = line_meta["title"],
        path = "/%s/" % folder
    )
    #print(target_file)
    ut.write_json_file(target_file, data )

    datapth = os.path.join(DATA_PATH, "%s.json" % folder.replace("-", "_"))
    print("dataaaaaaaa=", datapth)
    subprocess.run(["ln", "-s", "-f", target_file, datapth])

    s = "var DATA = %s " % json.dumps(data)
    target_file = os.path.join(PROJECT_PATH, folder, "data.js")
    ut.write_file(target_file, s)



def read_frontmatter(section, folder, page="index.md"):
    ret = {}
    pth = os.path.join(section, folder, page)
    print("PTH=", pth)
    if not os.path.exists(pth):
        return None, "Not EXIST %s" % pth



    with open(pth) as f:
        data = frontmatter.load(f)
        #print("data=", data.keys())
        for ki in data.keys():
            ret[ki] = data[ki]

        #n = data['name']

        # lst.append(dict(path=sdir, name=n))
        ret['meta'] = dict(src=pth,
                    path =  "/station/%s/" % folder
                    )

        return ret, None



def load_disused_cache():

    global disusedCache
    disu_list, err = ut.read_json_file(DISUSED_CACHE_FILE)
    disusedCache = {}
    for rec in disu_list:
        if rec['name'] is not None:
            disusedCache[rec['name'].lower()] = rec





def fetch_disused():

    # http://disused-stations.org.uk/sites_a.shtml

    lst = []


    for idx, cc in enumerate(ut.a2z()):
        url = "%s/sites_%s.shtml" % (DISUSED_URL, cc)

        print(url)
        r = requests.get(url)

        if r.status_code != 200:
            print(r.status_code)
            continue
        #print(r.text)


        soup = bs4.BeautifulSoup(r.text, 'html.parser')
        #print(soup.title)

        links = soup.find_all('a')
        for link in links:
            #print(link)
            href = link['href']
            if href.startswith("%s/" % cc):
                dic = dict(
                    name = link.string,
                    url = "%s/%s" % (DISUSED_URL, href)
                )
                lst.append(dic)
                if not link.string:
                    print("====", link)
        if idx == 3:
            #break
            pass


    ut.write_json_file(DISUSED_CACHE_FILE, lst)


def get_disused(sta):
    global disusedCache
    if disusedCache == None:
        load_disused_cache()

    if disusedCache == None:
        oops()
        return None, "Failed load cache"

    for ki, val in disusedCache.items():
        if sta in ki:
            return val, None
    return None, None

def get_kml_path(folder):
    fp = os.path.join(PROJECT_PATH, folder)
    return "%s/%s.kml" % (fp, folder)

def kml_to_geojson(folder):

    fp = os.path.join(PROJECT_PATH, folder)
    kml_path = "%s/%s.kml" % (fp, folder)
    temp_target = os.path.join(TEMP_PATH, "%s.geojson" % folder, )
    final_target = "%s/data.geojson" % (fp)


    print(fp)
    print(kml_path)
    if os.path.exists(kml_path):
        #subprocess.run(["ls", "-l"])
        subprocess.run(["k2g", kml_path, TEMP_PATH])

        data, err = process_geojson(src=temp_target, dest=final_target)

        return data, err

    return False


def process_geojson(src=None, dest=None):

    with open(src, "r", buffering=10000000) as f:
        data = json.load(f)


        stations = []
        lines = []
        lst = []


        for feat in data['features']:
            #print("deat==", feat)
            goem = feat['geometry']
            typ = goem['type']


            if typ == "Point":

                latlng = goem['coordinates']

                dic = dict(title=feat['properties']['name'],
                           #type="station",
                           #status="closed",
                           info= dict(position=dict(lat=latlng[0], lng=latlng[1]))

                           )

                #print(">>>>>>>>>", dic['disused'])
                stations.append(dic)
                lst.append(feat)

            elif typ == "LineString" :
                #if not "FLY" in feat['properties']['name'].upper():
                lines.append(feat)
                lst.append(feat)

            elif typ in ["Polygon"]:
                pass

            else:

                print(typ, feat)
                panic()
    print("WRITE=", dest)
    ut.write_json_file(dest, lst)





    return dict(stations=stations, lines=lines), None

KML_START = """<?xml version="1.0" encoding="UTF-8"?>
<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom">
<Folder>
	<name>wr</name>
	<open>1</open>"""

KML_END = """</Folder>
</kml>
"""

TPL = """   <NetworkLink>
		<name>{title}</name>
		<open>1</open>
		<description><![CDATA[More at <a href="{www}">welsh-railways/{folder}</a>]]></description>
		<Link>
			<href>http://localhost:82/{folder}/{folder}.kml</href>
		</Link>
	</NetworkLink>
"""


def write_stations_index():

    idxx = {}


    for rec in read_stations(False):
        c = rec['title'][0].upper()
        if not c in idxx:
            idxx[c] = []
        lnk = "- [%s](%s)\n" % ( rec['title'], rec['meta']['path'])
        idxx[c].append(lnk)

    s = "Stations Index\n================\n\n"

    # s += "\n".join(toc)
    for aa in sorted(idxx.keys()):
        s += "## %s\n\n" % aa


        s += "\n".join(idxx[aa])
        s += "\n\n"

    s += "\n\n"

    stations_md = os.path.join(PROJECT_PATH, "stations.md")
    print(s)
    ut.write_file(stations_md, s)

def read_stations(lookup=True):
    lst = []
    for folder in sorted(os.listdir(STATIONS_PATH)):
        print(folder)

        data, err = read_frontmatter(STATIONS_PATH, folder)
        print(data.keys())
        if lookup:
            lst.append(dict(title=data['title'],
                            position = data['info']['position'],
                            path="/station/%s/" % folder))
        else:
            lst.append(data)

    return lst


def gen_site_not_human():



    lines = []
    gdata = []

    features = []

    for folder in os.listdir(PROJECT_PATH):
        kmlf = os.path.join(PROJECT_PATH, folder, "%s.kml" % folder)
        if os.path.exists(kmlf):
            print(folder)
            meta, err = read_frontmatter(PROJECT_PATH, folder)
            #print("meta==", meta)
            dic = dict(folder=folder, www="%s/%s/" % (WWW_ROOT, folder),
                       title=meta['title'])
            lines.append(dic)


            dic = dict(geojson="%s/%s/data.geojson" % (WWW_ROOT, folder),
                       title=meta['title'])

            gdata.append(dic)

            pp = os.path.join(PROJECT_PATH, folder, "data.geojson")
            featuu, err = ut.read_json_file(pp)
            features.extend(featuu)



    ## json
    target = os.path.join(PROJECT_PATH, "index.json")
    print("piut==", target)
    ut.write_json_file(target, dict(lines=gdata, stations=read_stations()))


    target = os.path.join(PROJECT_PATH, "index.geojson")
    ut.write_json_file(target, features)




    s = KML_START

    for lin in lines:
        #print(lin)
        s += TPL.format(**lin)

    s += KML_END
    #print(s)
    target = os.path.join(PROJECT_PATH, "index.kml" )
    print("piut==", target)
    ut.write_file(target, s)

"""

	<NetworkLink>
		<name>Carms Llandeilo</name>
		<open>1</open>
		<description><![CDATA[This is desctipion <a href="http:// link"> here</a>]]></description>
		<Link>
			<href>http://localhost:82/carmarthen-llandeilo/carmarthen-llandeilo.kml</href>
		</Link>
	</NetworkLink>
	<NetworkLink>
		<name>Untitled Network Link</name>
		<open>1</open>
		<Link>
			<href>http://localhost:82/anglesey-central/anglesey-central.kml</href>
		</Link>
	</NetworkLink>


"""