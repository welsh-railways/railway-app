
# -*- coding: utf-8 -*-
# @author: Peter Morgan <pete@daffodil.uk.com>

import os

from Qt import QtGui, QtCore, Qt, QtWidgets, pyqtSignal

import frontmatter

import xwidgets
import xtree
from img import Ico
import xmodels
import xtree
import railib

class C:
    title = 0
    url = 1

class WikipediaModel( xmodels.XDictModel ):

    #sigFolder = pyqtSignal(object)

    def __init__( self, parent=None):
        super().__init__()


        self.init_col_defs(C)


class Wikipedia( QtWidgets.QWidget ):

    sigFolder = pyqtSignal(object)

    def __init__( self, parent=None):
        super().__init__()


        self.mainLayout = xwidgets.vlayout()
        self.setLayout( self.mainLayout )


        self.toolbar = xwidgets.XToolBar()
        self.mainLayout.addWidget(self.toolbar)

        self.txtSearch = xwidgets.XLineEdit()
        self.toolbar.addWidget(self.txtSearch)
        self.txtSearch.setText("llandeilo station")

        self.buttSearch = xwidgets.XToolButton(clicked=self.on_search, text="Go", ico=Ico.save)
        self.toolbar.addWidget(self.buttSearch)


        #self.actUpdateIndex = self.toolbar.addAction( "Stations.md", self.write_stations_index)
        #self.actUpdateIndex = self.toolbar.addAction("Up Disused", self.up_disused)
        #self.actUpdateIndex = self.toolbar.addAction("Site.idx", self.on_write_site_kml)
        #self.actUpdateIndex = self.toolbar.addAction("Site.idx", self.wr)

        #self.actUpdateIndex = self.toolbar.addAction("Check Disused", self.check_disused)

        #self.actWiki = self.toolbar.addAction("Wiki", self.wiki_search)
        self.model = WikipediaModel()

        self.tree = xtree.XTreeView()
        self.mainLayout.addWidget(self.tree)

        self.tree.setModel(self.model)


    def on_search(self):

        s = self.txtSearch.s()
        if not s:
            return

        recs, err = railib.wiki_search(s)

        self.model.set_recs(recs)
        print(recs)



