""" This module contains the 'index' Blueprint which organize and
group, views related to the index endpoint of HTTP REST API.
"""


from flask import Blueprint, jsonify, make_response, render_template


bp = Blueprint('index', __name__, url_prefix='')

from app.database import Base, db_session

from app.model.models import SRID, Station, Rail

# @bp.route('/', methods=['GET'])
# def index() -> str:
#
#     data = dict(success=True)
#
#     return render_template("BASE.html")

@bp.route('/', methods=('GET',))
def alive():

    q = db_session.query(
        Station.s_id, Station.name,
        Station.position
    )
    print(q.all())

    return make_response(jsonify({
        'status': 'success',
        'alive':True,

    }), 200)

@bp.route('/import_kml', methods=('GET',))
def import_kml():

    data = {}

    NS = "{http://www.opengis.net/kml/2.2}"
    pth = "/home/railways/welsh-railways.gitlab.io/docs/"

    origin = "anglesey-central/anglesey-central.kml"

    fn = pth + origin

    print(fn)
    import xml.etree.ElementTree as et

    doc = et.parse(fn)

    NS = '{http://www.opengis.net/kml/2.2}'

    print("------------------------------")
    for pm in doc.iterfind('.//{0}Placemark'.format(NS)):
        name = pm.find('{0}name'.format(NS)).text
        print("# >>" , name )
        slug = name.lower()

        # #.iterfind('.//{0}Placemark'.format(NS)):
        # lineStrings = doc.findall('.//{0}LineString'.format(NS))# './/{http://earth.google.com/kml/2.1}LineString')
        # for attributes in lineStrings:
        #     for subAttribute in attributes:
        #         if subAttribute.tag == '{http://earth.google.com/kml/2.1}coordinates':
        #             print(subAttribute.tag, subAttribute.text)

        lines = pm.findall('{0}LineString/{0}coordinates'.format(NS))
        #for ls in pm.iterfind('{0}MultiGeometry/{0}LineString/{0}coordinates'.format(nmsp)):
        #print("   lines=", lines)
        if len(lines):
            pp = []
            for ls in lines:

                points = ls.text.strip().replace('\n','').split(" ")
                for p in points:
                    parts =  p.split(",")
                    #print("   p=", parts)
                    pp.append("%s %s" % (parts[0], parts[1]))
            #self.position = "SRID=%s;POINT(%s %s)" % (SRID, lat, lng)

            s ="SRID=%s;LINESTRING(%s)" % (SRID, ",".join(pp))

            rail = db_session.query(Rail
                                    ).filter(Rail.origin == origin
                                    ).filter(slug == slug).first()
            if rail == None:
                rail = Rail(origin=origin, position=s)
                db_session.add(rail)

            db_session.commit()

        stations = pm.findall('{0}Point/{0}coordinates'.format(NS))
        print("  stations=", len(stations))
        if len(stations):

            sta = stations[0]
            print("   staaa=", sta.text)
            point = sta.text.strip().replace('\n', '').split(",")
            #p = (points[0], points[1])
            #parts = points.split(",")
            print("   point=", point)
            ps = "%s %s" % (point[0], point[1])
            posi = "SRID=%s;Point(%s)" % (SRID, ps)

            sta = db_session.query(Station
                                    ).filter(Station.origin == origin
                                             ).filter(slug == slug).first()
            #if sta == None:
            sta = Station(slug=slug, origin=origin)
            db_session.add(sta)
            sta.name = name
            sta.position = posi

            db_session.commit()



    q = db_session.query(
        Station.s_id, Station.name,
        Station.position
    )
    print(q.all())

    return make_response(jsonify(data), 200)