"""This package is Flask HTTP REST API Template template that already has the database bootstrap
implemented and also all feature related with the user authentications.

Application features:
    Python 3.7
    Flask
    PEP-8 for code style

This module contains the factory function 'create_app' that is
responsible for initializing the application according
to a previous configuration.
"""


import os

from flask import Flask, request, session, abort, make_response, jsonify
from flask_session import Session



def init_blueprints(app: Flask) -> None:
    """Registes the blueprint to the application.

    Parameters:
        app (flask.app.Flask): The application instance Flask that'll be running
    """

    # error handlers
    from .blueprint.handlers import register_handler
    register_handler(app)

    # error Handlers
    from .blueprint import index



    app.register_blueprint(index.bp)


def create_app(test_config: dict = {}) -> Flask:
    """This function is responsible to create a Flask instance according
    a previous setting passed from environment. In that process, it also
    initialise the database source.

    Parameters:
        test_config (dict): settings coming from test environment

    Returns:
        flask.app.Flask: The application instance
    """

    app = Flask(__name__, instance_relative_config=True)

    load_config(app, test_config)
    app.debug = True


    Session(app)
    init_instance_folder(app)
    init_database(app)
    init_blueprints(app)
    init_commands(app)



    return app


def load_config(app: Flask, test_config) -> None:
    """Load the application's config

    Parameters:
        app (flask.app.Flask): The application instance Flask that'll be running
        test_config (dict):
    """
    f_env = os.environ.get('FLASK_ENV')
    assert f_env != None

    if f_env == 'dev': # or test_config.get("FLASK_ENV") == 'pedro':
        app.config.from_object('app.config.Dev')


    else:
        assert 1 == 0

    #print(app.config)
    #app.config['SQLALCHEMY_ECHO'] = True

def init_instance_folder(app: Flask) -> None:
    """Ensure the instance folder exists.

    Parameters:
        app (flask.app.Flask): The application instance Flask that'll be running
    """
    #print("app.instance_path", app.instance_path)
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass


def init_database(app) -> None:
    """Responsible for initializing and connecting to the database
    to be used by the application.

    Parameters:
        app (flask.app.Flask): The application instance Flask that'll be running
    """

    from .database import init
    init(app)



def init_commands(app):
    from app.commands import register_commands
    register_commands(app)

