
import uuid
import datetime
import decimal
from sqlalchemy.sql import sqltypes
from sqlalchemy import func
import geoalchemy2
#from geoalchemy2 import functions as geo2funks
from geoalchemy2 import types as geo2types
from shapely import wkb

import shortuuid

## Short version
def gen_short_uuid():
    return shortuuid.uuid()

def gen_uuid4():
    return str(uuid.uuid4())

def gen_uuid(short=False):
    if short:
        return gen_short_uuid()
    return gen_uuid4()

DB_DATE_FORMAT = "%Y-%m-%d"
DB_DATE_TIME_FORMAT = "%Y-%m-%d %H:%M:%S"
DB_TIME_FORMAT = "%H:%M:%S"

def now():
    return datetime.datetime.utcnow()

def to_int(s, default=None):
    if s == None:
        return default
    try:
        return int(s)
    except:
        pass
    return default


def to_json_value(v):

    if isinstance(v, (str, bool)):
        return v

    elif isinstance(v, datetime.datetime):
        return v.strftime(DB_DATE_TIME_FORMAT)
        return str(v)

    elif isinstance(v, datetime.date):
        #return v.isoformat()
        return str(v)

    elif isinstance(v, geoalchemy2.elements.WKBElement):
        return None

    else:
        return str(v)


    return v

def object2dict(cls, cols):

    ret = {}
    for idx, col in enumerate(cols):
        val = cls[idx]
        typ = col['type']


        # handle primitives
        if isinstance(typ, (sqltypes.String, sqltypes.Integer, sqltypes.Boolean)):

            ret[col['name']] = val

        elif isinstance(typ, (sqltypes.Date)):
            if val == None:
                ret[col['name']] = None
            else:
                ret[col['name']] = val.strftime(DB_DATE_FORMAT)

        elif isinstance(typ, (sqltypes.DateTime)):
            if val == None:
                ret[col['name']] = None
            else:
                ret[col['name']] = val.strftime(DB_DATE_TIME_FORMAT)

        elif isinstance(typ, sqltypes.Numeric):
            #print("typ numeric==", typ, type(val), val)
            if val == None:
                ret[col['name']] = None
            else:
                if isinstance(val, decimal.Decimal):
                    ret[col['name']] = float(val)
                else:
                    ret[col['name']] = val

        elif isinstance(typ, (geo2types.Geometry)):
            if val is None:
                ret[col['name']] = None

            else:
                point = wkb.loads(bytes(val.data))
                ret[col['name']] = dict(lat=point.x, lng=point.y)


        else:
            print("typ unhandled==", typ, type(val), val)
            for c in val.__table__.columns:
                ret[c.name] = to_json_value(getattr(val, c.name))


    return ret


def objects2dict(recs, cols=None):
    return [object2dict(rec, cols) for rec in recs]



def split(s: str, splitter=","):
    ret = []
    parts = s.split(splitter)
    for p in parts:
        if p.strip() != "":
            ret.append(p.strip())
    return ret

