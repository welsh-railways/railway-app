"""This module contains class whose instances will be used to
load the settings according to the running environment. """


import os

#from dotenv import load_dotenv


class DefaultConfig():
    """Class containing the default settings for all environments.

    Constants:
        SQLALCHEMY_TRACK_MODIFICATIONS (boolean): signals to get notified
        before and after changes are committed to the database.
    """
    SQLALCHEMY_ECHO = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    DEBUG = False
    TESTING = False

    JWT_BLACKLIST_ENABLED = True
    JWT_BLACKLIST_TOKEN_CHECKS = ['access', 'refresh']

    #SESSION_TYPE = 'filesystem'
    #SESSION_COOKIE_NAME = "karma"
    #SESSION_COOKIE_SECURE = False


# PEDROOOOOO
class Dev(DefaultConfig):


    #load_dotenv()  # loading .env
    # zzzz zzzzzza
    SQLALCHEMY_ECHO = False
    DEBUG = True
    FLASK_DEBUG = True
    TESTING = True
    SECRET_KEY = 'this_is_21312s_Sadgtrgd'


    SQLALCHEMY_DATABASE_URI = "postgresql+psycopg2://mash:mash@127.0.0.1:5433/wr"


