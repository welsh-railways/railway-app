"""This module define all models (persistent objects - PO) of application. Each model
is a subclasse of the Base class (base declarative) from app.model.database module.
The declarative extension in SQLAlchemy allows to define tables and models in one go,
that is in the same class.
"""


from datetime import datetime

import bcrypt
from sqlalchemy import inspect, func
from sqlalchemy import Column, Integer, String, Boolean, DateTime, Date, Time, Float, Numeric, Text, CHAR, ForeignKey
from sqlalchemy.event import listens_for


from geoalchemy2 import Geometry

from app.database import Base, db_session
import app.helpers as h

DEFAULT_LAT_LON = [51.804619, -4.072547]



#====================================================================================

SRID = 4326

class Station(Base):
    __tablename__ = 'stations'

    s_id = Column(Integer, primary_key=True)
    name = Column(String(length=100), index=True)
    description = Column(String(length=255))
    xrtype = Column(String(length=20), index=True)
    origin = Column(String(length=255), index=True)
    slug = Column(String(length=30), index=True)

    position = Column(Geometry('POINT', srid=SRID), nullable=True)
    position_valid = Column( Boolean(), index=True)
    wikipedia = Column(Boolean(), index=True)
    #lat = Column(Float(), nullable=True)
    #lng = Column(Float(), nullable=True)

    date_open = Column(Date(), index=True)
    date_close = Column(Date(), index=True)

    next = Column(Integer, index=True)
    previous = Column(Integer, index=True)

    def set_lat_lng(self, lat, lng):
        self.position = "SRID=%s;POINT(%s %s)" % (SRID, lat, lng)



class Rail(Base):
    __tablename__ = 'rails'

    l_id = Column(Integer, primary_key=True)
    name = Column(String(length=100), index=True)
    xrtype = Column(String(length=20), index=True)
    origin = Column(String(length=255), index=True)
    slug = Column(String(length=30), index=True)

    position = Column(Geometry('Linestring', srid=SRID), nullable=True)
