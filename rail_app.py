#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@copyright: Peter Morgan <pete@daffodil.uk.com>
"""

import sys, os
import argparse

from PyQt5 import QtGui, QtCore, QtWidgets


#from railapp import G
import railapp.main_window


parser = argparse.ArgumentParser(description="Launch desktop application")
parser.add_argument("--pedro", help="Pete's Developer mode", action="store_true")


if __name__ == '__main__':


    args = parser.parse_args()


    ## Qt GO!
    app = QtWidgets.QApplication(sys.argv)


    ## Splash Screen
    #if G.settings.is_dev() == False:
    #    #splashScreen = carma.main.main_window.show_splash()
    #    pass
    app.processEvents()


    ## Main Window
    window = railapp.main_window.MainWindow()


    ## Show and run or getta outta here.. its a nightmare.. in front of your in daylight.. coloured screen
    #if G.settings.is_dev() == False:
    #    pass #splashScreen.finish( window )
    window.show()

    sys.exit( app.exec_() )

